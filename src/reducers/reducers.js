import { combineReducers } from 'redux'
import Food from './Food'

const reducers = combineReducers({
    food : Food
})

export default reducers;