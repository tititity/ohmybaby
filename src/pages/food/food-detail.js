import React, { Component } from 'react'
import { View, Text, StyleSheet, Image} from 'react-native'
import { connect } from 'react-redux'


class FoodDetail extends Component {
    
    state = {
        food : this.props.food 
    }

    componentDidMount(){
        console.log('title',this.props)
    }
    render(){
        return(
            <View>
                  <Image
                  source = {require('../../img/food/fat-cover.jpg')}
                  style={styles.cover}/>
                  <View>
                      <Text>{this.state.title}</Text>
                  </View>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    cover : {
        width: '100%',
        height: 237
    }
})

const mapStateToProps = (state) => {
    return{
        food: state.food
    }
}

export default connect(mapStateToProps)(FoodDetail);