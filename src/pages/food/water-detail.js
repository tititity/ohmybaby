import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, ScrollView, TouchableOpacity, ImageBackground } from 'react-native'

class WaterDetail extends Component {

    onBack = () => {
        this.props.history.push('/food-menu')
    }
    render() {
        return (
            <ScrollView>
                <View style={{ alignItems: 'center', marginBottom: 25 }}>
                    <ImageBackground
                        source={require('../../img/food/water-cover.png')}
                        style={styles.cover} >

                        <TouchableOpacity
                            onPress={() => { this.onBack() }}
                            style={styles.touchBack}>
                            <Text style={styles.text}>
                                กลับ
                                </Text>
                        </TouchableOpacity>

                        <View style={{
                            backgroundColor: 'white', borderRadius: 10, width: 170, height: 50, justifyContent: 'center', alignItems: 'center',
                            position: 'absolute', left: 93, top: 210,
                            elevation: 3
                        }}>
                            <Text style={styles.title}>น้ำ</Text>
                        </View>
                    </ImageBackground>
                </View>

                <View style={{ margin: 20 }}>
                    <Text style={styles.text}>
                        {'    '}เป็นส่วนประกอบที่จำเป็นที่สุดในการทำงานของร่างกายถ้าขาดน้ำจะทำให้ร่างกายไม่สามารถทำงานได้ปกติ
                        น้ำช่วยให้ส่วนต่างๆของร่างกายทำงานได้เป็นปกติและช่วยเพิ่มปริมาณน้ำในหลอดเลือดทำให้การนำสารอาหารไปเลี้ยงส่วนต่างๆของร่างกายได้ดีขึ้น
                        ถ้าหากคุณแม่มีภาวะขาดน้ำจะทำให้เสี่ยงต่อการทำให้มดลูกมีการหดรัดตัวและนำไปสู่การเจ็บครรภ์คลอดก่อนกำหนดได้ ดังนั้นคุณแม่ควรดื่มน้ำวันละประมาณ 6-8 แก้ว
                      </Text>
                </View>
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    cover: {
        width: '100%',
        height: 237
    },
    text: {
        fontFamily: 'NotoSansThai-Regular',
        fontSize: 15
    },
    textbold: {
        fontFamily: 'NotoSansThai-Bold',
        fontSize: 16
    },
    title: {
        fontFamily: 'NotoSansThai-Regular',
        fontSize: 18
    },
    touchBack: {
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        left: 8,
        top: 10,
        width: 50,
        height: 50,
        borderRadius: 23,
        backgroundColor: 'rgba(247, 247, 247, 0.5)'
    }
})

export default WaterDetail