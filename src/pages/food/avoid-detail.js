import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, ScrollView, TouchableOpacity, ImageBackground } from 'react-native'
import { Card } from 'react-native-elements'

class AvoidDetail extends Component {

    onBack = () => {
        this.props.history.push('/food-menu')
    }

    render() {
        return (
            <ScrollView>
                <View style={{ alignItems: 'center', marginBottom: 25 }}>
                    <ImageBackground
                        source={require('../../img/food/avoid-cover.png')}
                        style={styles.cover} >

                        <TouchableOpacity
                            onPress={() => { this.onBack() }}
                            style={styles.touchBack}>
                            <Text style={styles.text}>
                                กลับ
                                </Text>
                        </TouchableOpacity>

                        <View style={{
                            backgroundColor: 'white', borderRadius: 10, width: 170, height: 50, justifyContent: 'center', alignItems: 'center',
                            position: 'absolute', left: 93, top: 210,
                            elevation: 3
                        }}>
                            <Text style={styles.title}>อาหารที่ควรหลีกเลี่ยง</Text>
                        </View>
                    </ImageBackground>
                </View>

                <View style={{ margin: 20 }}>
                    <Text style={styles.text}>
                        <Text style={styles.textbold}>อาหาร เครื่องดื่มและสารเสพติดที่ควรหลีกเลี่ยง</Text> {'\n'}{'\n'} 
                        {'      '}1. อาหารรสจัดของหมักดองและอาหารที่มีสารเคมีปนเปื้อนเพราะอาจจะทำให้คุณแม่ท้องเสียและลูกน้อยได้รับอันตราย{'\n'}
                        {'      '}2. เครื่องดื่มที่มีส่วนผสมของแอลกอฮอล์เพราะอาจเพิ่มความเสี่ยงต่อการแท้งบุตรลูกน้อยในครรภ์เจริญเติบโตช้าปัญญาอ่อนและพิการ{'\n'}
                        {'      '}3. เครื่องดื่มประเภทชาเพราะอาจทำให้ท้องผูก{'\n'}
                        {'      '}4. กาแฟมีคาเฟอีนซึ่งเป็นสารกระตุ้นทำให้นอนไม่หลับใจสั่น{'\n'}
                        {'      '}5. บุหรี่มีสารนิโคตินซึ่งสามารถผ่านรกไปสู่ทารกในครรภ์ได้มีผลทำให้กดการทำงานของสมองและอาจทำให้ลูกน้อยเจริญเติบโตช้า{'\n'}
                        และเกิดการคลอดก่อนกำหนด
                      </Text>
                </View>

                <View style={{ margin: 20 }}>
                    <Text style={styles.textbold}>ตัวอย่างอาหารที่ควรหลีกเลี่ยง</Text>
                    <ScrollView horizontal={true}>
                        <View style={{ margin: 5, flexDirection: 'row' }}>
                            <Card
                                title="ของหมักของดอง"
                                image={require('../../img/food/dong.jpg')}
                                imageStyle={{ height: 300, width: 300, margin: 3 }} />
                            <Card
                                title="เครื่องดื่มแอลกอฮอล์"
                                image={require('../../img/food/alchohol.jpg')}
                                imageStyle={{ height: 300, width: 300, margin: 3 }} />
                            <Card
                                title="กาแฟ"
                                image={require('../../img/food/coffee.jpg')}
                                imageStyle={{ height: 300, width: 300, margin: 3 }} />
                            <Card
                                title="บุหรี่"
                                image={require('../../img/food/cigarette.jpg')}
                                imageStyle={{ height: 300, width: 300, margin: 3 }} />
                        </View>
                    </ScrollView>
                </View>
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    cover: {
        width: '100%',
        height: 237
    },
    text: {
        fontFamily: 'NotoSansThai-Regular',
        fontSize: 15
    },
    textbold: {
        fontFamily: 'NotoSansThai-Bold',
        fontSize: 16
    },
    title: {
        fontFamily: 'NotoSansThai-Regular',
        fontSize: 18
    },
    touchBack: {
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        left: 8,
        top: 10,
        width: 50,
        height: 50,
        borderRadius: 23,
        backgroundColor: 'rgba(247, 247, 247, 0.5)'
    }
})

export default AvoidDetail;