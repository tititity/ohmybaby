import React, { Component } from 'react'
import { View, Text, StyleSheet, Image, ScrollView, TouchableOpacity, ImageBackground } from 'react-native'
import { Card } from 'react-native-elements';

class ProteinDetail extends Component {

    onBackPress = () => {
        return false;
    }
    onBack = () => {
        this.props.history.push('/food-menu')
    }

    render() {
        return (
                <ScrollView>
                <View style={{ alignItems: 'center', marginBottom: 25 }}>
                        <ImageBackground
                            source={require('../../img/food/protein-cover.jpg')}
                            style={styles.cover} >

                            <TouchableOpacity
                                onPress={() => { this.onBack() }}
                                style={styles.touchBack}>
                                <Text style={styles.text}>
                                    กลับ
                                </Text>
                            </TouchableOpacity>

                            <View style={{
                                backgroundColor: 'white', borderRadius: 10, width: 170, height: 50, justifyContent: 'center', alignItems: 'center',
                                position: 'absolute', left: 93, top: 210,
                                elevation: 3
                            }}>
                                <Text style={styles.title}>โปรตีน</Text>
                            </View>
                        </ImageBackground>
                    </View>

                    <View style={{ margin: 20 }}>
                        <Text style={styles.text}>
                            มีความสำคัญในการสร้างอวัยวะและส่งเสริมการเจริญเติบโตของร่างกายทารกในครรภ์ รก มดลูก เต้านมและการเพิ่มขึ้นของปริมาณเลือด
                            รวมทั้งช่วยซ่อมแซมส่วนที่สึกหรอของร่างกาย คุณแม่จึงควรรับประทานโปรตีนเพิ่มขึ้นมากกว่าปกติวันละ 7 กรัมหรือรับประทานโปรตีนประมาณ 1.5 กรัม
                            ต่อน้ำหนัก 1 กิโลกรัมต่อวันโดยเฉพาะในระยะ 3 เดือนก่อนคลอดเพราะเป็นช่วงที่ลูกน้อยในครรภ์เจริญเติบโตมากที่สุด หากได้รับไม่เพียงพออาจทำให้คุณแม่เกิดภาวะโลหิตจางได้
                            และขณะเดียวกันอาจทำให้ลูกมีเซลล์สมองน้อย สติปัญญาต่ำ อาหารที่มีโปรตีนมากได้แก่ เนื้อสัตว์ต่างๆ ไข่ นม ถั่ว เต้าหู้
                      </Text>
                    </View>

                    <View style={{ margin: 20 }}>
                        <Text style={styles.textbold}>ตัวอย่างอาหาร</Text>
                        
                            <View style={{ margin: 5 }}>
                                <Card
                                    title="เนื้อสัตว์ต่างๆ"
                                    image={require('../../img/food/meat2.jpg')}
                                    imageStyle={{ height: 300, margin: 3 }} />
                                <Card
                                    title="ไข่"
                                    image={require('../../img/food/egg.jpg')}
                                    imageStyle={{ height: 300, margin: 3 }} />
                                <Card
                                    title="นม"
                                    image={require('../../img/food/milk.jpg')}
                                    imageStyle={{ height: 300, margin: 3 }} />
                                <Card
                                    title="ถั่ว"
                                    image={require('../../img/food/nuts.jpg')}
                                    imageStyle={{ height: 300, margin: 3 }} />
                                <Card
                                    title="เต้าหู"
                                    image={require('../../img/food/tofu.jpg')}
                                    imageStyle={{ height: 300, margin: 3 }} />
                            </View>
                    </View>
                </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    cover: {
        width: '100%',
        height: 237
    },
    text: {
        fontFamily: 'NotoSansThai-Regular',
        fontSize: 15
    },
    textbold: {
        fontFamily: 'NotoSansThai-Bold',
        fontSize: 16
    },
    title: {
        fontFamily: 'NotoSansThai-Regular',
        fontSize: 18
    },
    touchBack: {
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        left: 8,
        top: 10,
        width: 50,
        height: 50,
        borderRadius: 23,
        backgroundColor: 'rgba(247, 247, 247, 0.5)'
    }
})

export default ProteinDetail;