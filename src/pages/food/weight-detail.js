import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, ScrollView, TouchableOpacity, ImageBackground } from 'react-native'

class WeightDetail extends Component {

    onBack = () => {
        this.props.history.push('/food-menu')
    }
    render() {
        return (
            <ScrollView>
                <View style={{ alignItems: 'center', marginBottom: 25 }}>
                    <ImageBackground
                        source={require('../../img/food/weight-cover.png')}
                        style={styles.cover} >

                        <TouchableOpacity
                            onPress={() => { this.onBack() }}
                            style={styles.touchBack}>
                            <Text style={styles.text}>
                                กลับ
                                </Text>
                        </TouchableOpacity>

                        <View style={{
                            backgroundColor: 'white', borderRadius: 10, height: 50, width: 280, justifyContent: 'center', alignItems: 'center',
                            position: 'absolute', left: 40, top: 210,
                            elevation: 3
                        }}>
                            <Text style={styles.title}>น้ำหนักที่ควรเพิ่มขึ้นระหว่างตั้งครรภ์</Text>
                        </View>
                    </ImageBackground>
                </View>

                <View style={{ margin: 20 }}>
                    <Text style={styles.text}>
                        {'          '}น้ำหนักของมารดาก่อนการตั้งครรภ์ และน้ำหนักที่เพิ่มขึ้นระหว่างการตั้งครรภ์
                        น้ำหนักของมารดาก่อนการตั้งครรภ์ และน้ำหนักที่เพิ่มขึ้นระหว่างการตั้งครรภ์มีผลต่อน้ำหนักของทารกแรกคลอด 
                        และระยะเวลาของการตั้งครรภ์ โดยมารดาที่มีน้ำหนักก่อนการตั้งครรภ์น้อยและมีน้ำหนักที่เพิ่มขึ้นระหว่างการตั้งครรภ์น้อยนั้น 
                        มีความเสี่ยงมากขึ้นที่จะคลอดบุตรที่มีน้ำหนักแรกคลอดน้อย หรือคลอดก่อนกำหนดได้ 
                        สำหรับมารดาที่มีน้ำหนักก่อนการตั้งครรภ์มากก็มีความเสี่ยงที่จะคลอดบุตรที่มีน้ำหนักตัวมากกว่าเกณฑ์ อายุครรภ์เกินกำหนด 
                        และมีภาวะแทรกซ้อนทางสูติกรรมอื่น ๆ เพิ่มขึ้น
                        {'\n'}
                        {'\n'}
                        <Text style={styles.textbold}>ค่าดัชนีมวลกาย (BMI: Body Mass Index)</Text> เป็นค่าที่อาศัยความสัมพันธ์ระหว่างน้ำหนักตัวและส่วนสูง เป็นเกณฑ์ในการเปรียบเทียบการเปลี่ยนแปลงของน้ำหนักตัวที่เพิ่มขึ้นในระหว่างการตั้งครรภ์ สามารถคำนวณได้โดย นำน้ำหนักตัว(หน่วยเป็นกิโลกรัม) หารด้วย ส่วนสูงกำลังสอง (หน่วยเป็นเมตร)</Text>
                </View>
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    cover: {
        width: '100%',
        height: 237
    },
    text: {
        fontFamily: 'NotoSansThai-Regular',
        fontSize: 15
    },
    textbold: {
        fontFamily: 'NotoSansThai-Bold',
        fontSize: 16
    },
    title: {
        fontFamily: 'NotoSansThai-Regular',
        fontSize: 18
    },
    touchBack: {
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        left: 8,
        top: 10,
        width: 50,
        height: 50,
        borderRadius: 23,
        backgroundColor: 'rgba(247, 247, 247, 0.5)'
    }
})

export default WeightDetail