import React, { Component } from 'react'
import { View, Text, StyleSheet, Image, ScrollView, TouchableOpacity, ImageBackground } from 'react-native'
import { Card } from 'react-native-elements';

class VitaminDetail extends Component {

    onBackPress = () => {
        return false;
    }
    onBack = () => {
        this.props.history.push('/food-menu')
    }
    render() {
        return (
            <ScrollView>
                <View style={{ alignItems: 'center', marginBottom: 25 }}>
                    <ImageBackground
                        source={require('../../img/food/vitamin-cover.jpg')}
                        style={styles.cover} >

                        <TouchableOpacity
                            onPress={() => { this.onBack() }}
                            style={styles.touchBack}>
                            <Text style={styles.text}>
                                กลับ
                                </Text>
                        </TouchableOpacity>

                        <View style={{
                            backgroundColor: 'white', borderRadius: 10, width: 170, height: 50, justifyContent: 'center', alignItems: 'center',
                            position: 'absolute', left: 93, top: 210,
                            elevation: 3
                        }}>
                            <Text style={styles.title}>วิตามิน</Text>
                        </View>
                    </ImageBackground>
                </View>

                <View style={{ margin: 20 }}>
                    <Text style={styles.text}>
                        มีความสำคัญในกระบวนการย่อยอาหาร ส่งเสริมกลไกการป้องกันโรคและช่วยให้ระบบต่างๆของร่างกายทำงานได้ตามปกติ
                        ซึ่งการรับประทานอาหารให้ครบ 5 หมู่จะทำให้ได้รับวิตามินอย่างเพียงพอแล้ว
                        ไม่จำเป็นต้องรับประทานวิตามินเสริมเพราะการได้รับวิตามินบางชนิดมากเกินไปอาจมีผลเสียต่อร่างกาย
                        ตัวอย่างวิตามินที่สำคัญ ได้แก่{'\n'}
                        {'  '}1.วิตามินซี มีมากในผลไม้รสเปรี้ยว แตงโมและมะเขือเทศ{'\n'}
                        {'  '}2.วิตามินบี มีมากในนม เนื้อสัตว์ต่างๆ ตับ ข้าวซ้อมมือ และธัญพืช{'\n'}
                        {'  '}3.วิตามินเอ มีมากในตับ ผักใบเขียวและเหลือง พริก ผลไม้ นมและเนย{'\n'}
                        {'  '}4.วิตามินอี มีมากในน้ำมันพืช และผักใบเขียว
                      </Text>
                </View>

                <View style={{ margin: 20 }}>
                    <Text style={styles.textbold}>ตัวอย่างอาหาร</Text>
                    <View style={{ margin: 5 }}>
                        <Card
                            title="ผมไม้รสเปรี้ยว"
                            image={require('../../img/food/orange.jpg')}
                            imageStyle={{ height: 300, margin: 3 }} />
                        <Card
                            title="แตงโม"
                            image={require('../../img/food/watermelon.jpg')}
                            imageStyle={{ height: 300, margin: 3 }} />
                        <Card
                            title="มะเขือเทศ"
                            image={require('../../img/food/tomato.jpg')}
                            imageStyle={{ height: 300, margin: 3 }} />
                        <Card
                            title="นม และเนื้อสัตว์ต่างๆ"
                            image={require('../../img/food/milk&meats.jpg')}
                            imageStyle={{ height: 300, margin: 3 }} />
                        <Card
                            title="ตับ"
                            image={require('../../img/food/liver.jpg')}
                            imageStyle={{ height: 300, margin: 3 }} />
                        <Card
                            title="ข้าวซ้อมมือ"
                            image={require('../../img/food/rice.jpg')}
                            imageStyle={{ height: 300, margin: 3 }} />
                        <Card
                            title="ธัญพืช"
                            image={require('../../img/food/wholewheat.jpg')}
                            imageStyle={{ height: 300, margin: 3 }} />
                        <Card
                            title="ผลไม้"
                            image={require('../../img/food/fruits.jpg')}
                            imageStyle={{ height: 300, margin: 3 }} />
                        <Card
                            title="น้ำมันพิช"
                            image={require('../../img/food/oil.jpg')}
                            imageStyle={{ height: 300, margin: 3 }} />

                    </View>
                </View>
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    cover: {
        width: '100%',
        height: 237
    },
    text: {
        fontFamily: 'NotoSansThai-Regular',
        fontSize: 15
    },
    textbold: {
        fontFamily: 'NotoSansThai-Bold',
        fontSize: 16
    },
    title: {
        fontFamily: 'NotoSansThai-Regular',
        fontSize: 18
    },
    touchBack: {
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        left: 8,
        top: 10,
        width: 50,
        height: 50,
        borderRadius: 23,
        backgroundColor: 'rgba(247, 247, 247, 0.5)'
    }
})

export default VitaminDetail;