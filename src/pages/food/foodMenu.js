import React, { Component } from 'react'
import { View, Text, ScrollView, TouchableOpacity, StyleSheet, Image } from 'react-native'
import { connect } from 'react-redux'
import { clickFat } from '../../system/Action'

class FoodMenu extends Component {

    state = {
        food: this.props.food
    }

    onClick = (text) => {
        switch (text) {
            case 'FAT':
                console.log('fat detail')
                this.props.history.push('/fat-detail')
                break;
            case 'PROTEIN':
                console.log('protain detail')
                this.props.history.push('/protein-detail')
                break;
            case 'VITAMIN':
                console.log('vitamin detail')
                this.props.history.push('/vitamin-detail')
                break;
            case 'MINERAL':
                console.log('mineral detail')
                this.props.history.push('/mineral-detail')
                break;
            case 'CARBO':
                console.log('carbo detail')
                this.props.history.push('/carbo-detail')
                break
            case 'WATER':
                console.log('water detail')
                this.props.history.push('water-detail')
                break
            case 'WEIGHT':
                console.log('weight-detail')
                this.props.history.push('/weight-detail')
                break
            case 'AVOID':
                console.log('avoid detail')
                this.props.history.push('/avoid-detail')
                break;

        }
    }

    onBack = () => {
        this.props.history.push('/main-menu')
    }

    render() {
        return (
            <View style={{ flex: 1, alignItems: 'center', marginTop: 10 }}>
                <View style={{ flexDirection: 'row', width: '100%', justifyContent: 'center', alignItems:'center' }}>
                        
                        <TouchableOpacity onPress={() => { this.onBack() }}
                            style={styles.touchBack}>
                            <Text style={styles.text}>
                                กลับ
                            </Text>
                        </TouchableOpacity>
                        <Text style={styles.title}>
                            อาหารสำหรับคุณแม่
                        </Text>

                </View>
                <TouchableOpacity onPress={() => { this.onClick('FAT') }}>
                    <Image
                        source={require('../../img/food/food-menu-fat.png')}
                        style={{ width: 95, height: 80 }} />
                </TouchableOpacity>
                <TouchableOpacity onPress={() => { this.onClick('PROTEIN') }}>
                    <Image
                        source={require('../../img/food/food-menu-protein.png')}
                        style={{ width: 175, height: 60, marginTop: 10 }} />
                </TouchableOpacity>
                <View style={{ flexDirection: 'row' }}>
                    <TouchableOpacity onPress={() => { this.onClick('VITAMIN') }}>
                        <Image
                            source={require('../../img/food/food-menu-vit.png')}
                            style={{ width: 115, height: 60, marginTop: 10, marginRight: 3 }} />
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => { this.onClick('MINERAL') }}>
                        <Image
                            source={require('../../img/food/food-menu-vege.png')}
                            style={{ width: 135, height: 60, marginTop: 10, marginLeft: 3 }} />
                    </TouchableOpacity>
                </View>
                <TouchableOpacity onPress={() => { this.onClick('CARBO') }}>
                    <Image
                        source={require('../../img/food/food-menu-carbo.png')}
                        style={{ width: 335, height: 60, marginTop: 10 }} />
                </TouchableOpacity>
                <TouchableOpacity onPress={() => { this.onClick('WATER') }}>
                    <Image
                        source={require('../../img/food/food-menu-water.png')}
                        style={{ width: 230, height: 50, marginTop: 40 }} />
                </TouchableOpacity>
                <TouchableOpacity onPress={() => { this.onClick('WEIGHT') }}>
                    <Image
                        source={require('../../img/food/food-menu-weight.png')}
                        style={{ width: 230, height: 50, marginTop: 15 }} />
                </TouchableOpacity>
                <TouchableOpacity onPress={() => { this.onClick('AVOID') }}>
                    <Image
                        source={require('../../img/food/food-menu-avoiding.png')}
                        style={{ width: 230, height: 50, marginTop: 15 }} />
                </TouchableOpacity>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    text: {
        fontFamily: 'NotoSansThai-Regular',
        fontSize: 15
    },
    textbold: {
        fontFamily: 'NotoSansThai-Bold',
        fontSize: 16
    },
    title: {
        fontFamily: 'NotoSansThai-Regular',
        fontSize: 18
    },
    touchBack: {
        justifyContent: 'center',
        alignItems: 'center',
        width: 50,
        height: 50,
        borderRadius: 23,
        backgroundColor: 'rgba(202, 202, 202, 0.26)',
        position: 'absolute',
        left: 10,
        top: 0

    }
})

const mapStateToProps = (state) => {
    return {
        food: state.food
    }
}
export default connect(mapStateToProps, { clickFat })(FoodMenu);