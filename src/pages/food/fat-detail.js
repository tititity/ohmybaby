import React, { Component } from 'react'
import { View, Text, StyleSheet, Image, ScrollView, TouchableOpacity, ImageBackground } from 'react-native'
import { Card } from 'react-native-elements';

class FatDetail extends Component {
    onBack = () => {
        this.props.history.push('/food-menu')
    }
    render() {
        return (
            <ScrollView>
                <View style={{ flex: 1 }}>

                    <View style={{ alignItems: 'center', marginBottom: 25 }}>
                        <ImageBackground
                            source={require('../../img/food/fat-cover.jpg')}
                            style={styles.cover} >

                            <TouchableOpacity
                                onPress={() => { this.onBack() }}
                                style={styles.touchBack}>
                                <Text style={styles.text}>
                                    กลับ
                                </Text>
                            </TouchableOpacity>

                            <View style={{
                                backgroundColor: 'white', borderRadius: 10, width: 170, height: 50, justifyContent: 'center', alignItems: 'center',
                                position: 'absolute', left: 93, top: 210,
                                elevation: 3
                            }}>
                                <Text style={styles.title}>ไขมัน</Text>
                            </View>
                        </ImageBackground>
                    </View>

                </View>


                <View style={{ margin: 20 }}>
                    <Text style={styles.text}>
                        มีความสำคัญในการให้พลังงานและความอบอุ่นแก่ร่างกาย ช่วยในการดูดซึมสารอาหารที่จำเป็นบางชนิด
                        คุณแม่ควรรับประทานอาหารกลุ่มนี้ให้น้อยลงเพราะเป็นอาหารที่ย่อยยาก อาจทำให้มีอาการท้องอืด แน่นท้องได้
                        อาหารเหล่านี้ยังเป็นไขมันชนิดที่มีกรดไขมันอิ่มตัว หากรับประทานมากอาจทำให้เกิดภาวะหลอดเลือดอุดตันได้
                        คุณแม่ควรเลือกรับประทานไขมันชนิดที่มีกรดไขมันไม่อิ่มตัวเพราะจำเป็นต่อการเจริญเติบโตของระบบประสาท
                        และสมองของลูกน้อยในครรภ์และไม่ทำให้เกิดการอุดตันในหลอดเลือด ได้แก่ ปลาทะเล สาหร่ายทะเล น้ำมันดอกคำฝอย
                    </Text>
                </View>

                <View style={{ margin: 20 }}>
                    <Text style={styles.textbold}>ตัวอย่างอาหาร</Text>
                    <View style={{ margin: 1 }}>
                        <Card
                            title="ปลาทะเล"
                            titleStyle={{ fontFamily: "NotoSansThai-Regular" }}
                            image={require('../../img/food/fishmeat.jpg')}
                            imageStyle={{ height: 300, margin: 3 }}
                            fontFamily="NotoSansThai-Regualr" />
                        <Card
                            title="สาหร่ายทะเล"
                            image={require('../../img/food/seaweed.jpg')}
                            imageStyle={{ height: 300, margin: 3 }} />
                        <Card
                            title="น้ำมันดอกคำฝอย"
                            image={require('../../img/food/khamfoi-oil.jpg')}
                            imageStyle={{ height: 300, margin: 3 }} />
                    </View>
                </View>
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    cover: {
        width: '100%',
        height: 237
    },
    text: {
        fontFamily: 'NotoSansThai-Regular',
        fontSize: 15
    },
    textbold: {
        fontFamily: 'NotoSansThai-Bold',
        fontSize: 16
    },
    title: {
        fontFamily: 'NotoSansThai-Regular',
        fontSize: 18
    },
    touchBack: {
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        left: 8,
        top: 10,
        width: 50,
        height: 50,
        borderRadius: 23,
        backgroundColor: 'rgba(247, 247, 247, 0.5)'
    }
})

export default FatDetail;