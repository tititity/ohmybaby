import React, { Component } from 'react'
import { View, Text, StyleSheet, Image, ScrollView, TouchableOpacity, ImageBackground } from 'react-native'
import { Card } from 'react-native-elements';

class MineralDetail extends Component {

    onBackPress = () => {
        return false;
    }
    onBack = () => {
        this.props.history.push('/food-menu')
    }
    render() {
        return (
            <ScrollView>
                {/* cover part */}
                <View style={{ alignItems: 'center', marginBottom: 25 }}>
                    <ImageBackground
                        source={require('../../img/food/vitamin-cover.jpg')}
                        style={styles.cover} >

                        <TouchableOpacity
                            onPress={() => { this.onBack() }}
                            style={styles.touchBack}>
                            <Text style={styles.text}>
                                กลับ
                            </Text>
                        </TouchableOpacity>

                        <View style={{
                            backgroundColor: 'white', borderRadius: 10, width: 170, height: 50, justifyContent: 'center', alignItems: 'center',
                            position: 'absolute', left: 93, top: 210,
                            elevation: 3
                        }}>
                            <Text style={styles.title}>แร่ธาตุ</Text>
                        </View>
                    </ImageBackground>
                </View>

                {/* detail */}
                <View style={{ margin: 20 }}>
                    <Text style={styles.text}>
                        มีความสำคัญอย่างยิ่งต่อสุขภาพของคุณแม่ การเจริญเติบโตและพัฒนาการของลูกน้อยในครรภ์ ซึ่งอาหารหลักที่รับประทานจะมีแร่ธาตุส่วนใหญ่อยู่อย่างเพียงพอ ไม่จำเป็นต้องรับประทานเพิ่ม
                        ตัวอย่างแร่ธาตุที่สำคัญ ได้แก่{'\n'}
                        {'  '}1.ธาตุเหล็ก มีมากในตับ ไข่แดง และผักใบเขียว{'\n'}
                        {'  '}2.แคลเซียม มีมากในนม เนย ปลาไส้ตัน งา และกุ้งแห้ง{'\n'}
                        {'  '}3.ไอโอดีน ฟอสฟอรัส สังกะสี แมกนีเซียม มีมากในอาหารทะเล{'\n'}
                        {'  '}4.โฟลิคหรือโฟเลท มีมากในผักใบเขียว ตับ ถั่วเหลือง ส้มและกล้วย{'\n'}
                    </Text>
                </View>

                {/* example */}
                <View style={{ margin: 20 }}>
                    <Text style={styles.textbold}>ตัวอย่างอาหาร</Text>
                    <ScrollView horizontal={true}>
                    <View style={{ margin: 5, flexDirection:'row'}}>
                        <Card
                            title="ตับ"
                            image={require('../../img/food/liver.jpg')}
                            imageStyle={{ height: 300, width:300, margin: 3 }} />
                        <Card
                            title="ไข่แดง"
                            image={require('../../img/food/egg-yolk.jpg')}
                            imageStyle={{ height: 300, width:300, margin: 3 }} />
                        <Card
                            title="เนย"
                            image={require('../../img/food/butter.jpg')}
                            imageStyle={{ height: 300, width:300, margin: 3 }} />
                        <Card
                            title="ปลาไส้ตัน"
                            image={require('../../img/food/anchovy.jpg')}
                            imageStyle={{ height: 300, width:300, margin: 3 }}/>
                        <Card
                            title="งา"
                            image={require('../../img/food/Sesame-Seed.jpg')}
                            imageStyle={{ height: 300, width:300, margin: 3 }}/>
                        <Card
                            title="กุ้งแห้ง"
                            image={require('../../img/food/koonghaeng.jpg')}
                            imageStyle={{ height: 300, width:300, margin: 3 }}/>
                        <Card
                            title="อาหารทะเล"
                            image={require('../../img/food/seafood.jpg')}
                            imageStyle={{ height: 300, width:300, margin: 3 }} />
                        <Card
                            title="ถั่วเหลือง"
                            image={require('../../img/food/soy.jpg')}
                            imageStyle={{ height: 300, width:300, margin: 3 }} />
                        <Card
                            title="ส้ม"
                            image={require('../../img/food/orange.jpg')}
                            imageStyle={{ height: 300, width:300, margin: 3 }}/>
                        <Card
                            title="กล้วย"
                            image={require('../../img/food/banana.jpg')}
                            imageStyle={{ height: 300, width:300, margin: 3 }}/>
                    </View>
                    </ScrollView>
                </View>

                <View style={{margin:20}}>
                    <Text style={styles.text}>
                        <Text style={styles.textbold}>ความต้องการแร่ธาตุต่างๆ</Text> {'\n'}{'\n'}
                        <Text style={styles.textbold}>1.ความต้องการธาตุเหล็ก</Text>{'\n'}
                            {'  '}ธาตุเหล็กเป็นสารอาหารที่จำเป็นในการช่วยสร้างเม็ดเลือดแดงเพื่อนำออกซิเจ
                            นไปเลี้ยงร่างกายลูกน้อยต้องการธาตุเหล็กเพิ่มมากขึ้นโดยเฉพาะในช่วงสามเ
                            ดือนก่อนคลอดเพื่อเก็บสะสมไว้ใช้ในระยะหลังคลอด
                            ถ้าหากคุณแม่ได้รับธาตุเหล็กไม่เพียงพอจะทำให้เกิดภาวะโลหิตจางเสี่ยงต่อการติดเ
                            ชื้อคลอดก่อนกำหนดและตกเลือดหลังคอดได้ง่ายดังนั้นคุณแม่ที่ไม่มีอาการแพ้ท้องแ
                            ล้วควรได้รับธาตุเหล็กเสริมวันละ 30
                            มิลลิกรัมและควรรับประทานธาตุเหล็กระหว่างมื้ออาหารพร้อมกับผลไม้หรือน้ำผลไม้ร
                            สเปรี้ยวเพราะธาตุเหล็กจะถูกดูดซึมได้ดีขณะท้องว่างและมีวิตามินซีช่วยในการดูดซึม 
                            ไม่ควรรับประทาน พร้อมนม กาแฟ ชาและไข่แดงหรือยาลดกรด
                            เพราะจะขัดขวางการดูดซึมธาตุเหล็ก{'\n'}
                            {'\n'}
                            <Text style={styles.textbold}>2.ความต้องการแคลเซียม</Text>{'\n'}
                            {'  '}ในระหว่างตั้งครรภ์คุณแม่ต้องการแคลเซียมมากกว่าปกติเพื่อใช้ในการสร้างกระดูกและการเจริญเติบโตของลูกน้อยในครรภ์
                            ลูกน้อยต้องการแคลเซียมมากขึ้นตามอายุครรภ์
                            ถ้าหากคุณแม่ขาดแคลเซียมจะทำให้เกิดโรคกระดูกบาง เป็นตระคริว
                            ปวดหลังและปวดกล้ามเนื้อได้โดยเฉพาะในระยะไตรมาสที่สามของการตั้งครรภ์
                            ดังนั้นคุณแม่จึงจำเป็นต้องรับประทานอาหารที่มีแคลเซียมสูงตลอดระยะของการตั้งครรภ์โดยควรได้รับแคลเซียม 1,200 มิลลิกรัมต่อวัน{'\n'}
                            {'\n'}
                            <Text style={styles.textbold}>3.ความต้องการไอโอดีน</Text>{'\n'}
                            {'  '}ในระหว่างตั้งครรภ์คุณแม่ต้องการไอโอดีนเพิ่มขึ้นเนื่องจากต่อมธัยรอยด์ทำงานมาก
                            ขึ้น ถ้าหากคุณแม่ขาดไอโอดีนมากอาจส่งผลให้ลูกน้อยในครรภ์เจริญเติบโตช้า
                            ร่างกายแคระแกรน สติปัญญาไม่ดี
                            ดังนั้นคุณแม่ควรรับประทานเกลือผสมไอโอดีนหรือเกลืออนามัย{'\n'}
                            {'\n'}
                            <Text style={styles.textbold}>4.ความต้องการโฟลิคหรือโฟเลท</Text>{'\n'}
                            {'  '}โฟลิคหรือโฟเลทมีความสำคัญในการพัฒนาสมองและระบบท่อประสาทของลูกน้อยในครรภ์ 
                            คุณแม่ควรได้รับโฟเลทเพิ่มขึ้นในช่วง 3 เดือนก่อนตั้งครรภ์และ 3
                            เดือนแรกของการตั้งครรภ์เพราะถ้าหากขาดโฟลิคอาจทำให้ลูกน้อยเกิดความผิดปกติแต่กำเนิดได้
                        </Text>
                </View>
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    cover: {
        width: '100%',
        height: 237
    },
    text: {
        fontFamily: 'NotoSansThai-Regular',
        fontSize: 15
    },
    textbold: {
        fontFamily: 'NotoSansThai-Bold',
        fontSize: 16
    },
    title: {
        fontFamily: 'NotoSansThai-Regular',
        fontSize: 18
    },
    touchBack: {
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        left: 8,
        top: 10,
        width: 50,
        height: 50,
        borderRadius: 23,
        backgroundColor: 'rgba(247, 247, 247, 0.5)'
    }
})

export default MineralDetail;