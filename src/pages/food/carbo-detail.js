import React, { Component } from 'react'
import { View, Text, StyleSheet, Image, ScrollView, TouchableOpacity, ImageBackground } from 'react-native'
import { Card } from 'react-native-elements';

class CarboDetail extends Component {

    onBackPress = () => {
        return false;
    }
    onBack = () => {
        this.props.history.push('/food-menu')
    }
    render() {
        return (
            <ScrollView>
                <View style={{ flex: 1 }}>

                    <View style={{ alignItems: 'center', marginBottom: 25 }}>
                        <ImageBackground
                            source={require('../../img/food/carbo-cover.jpg')}
                            style={styles.cover} >

                            <TouchableOpacity
                                onPress={() => { this.onBack() }}
                                style={styles.touchBack}>
                                <Text style={styles.text}>
                                    กลับ
                                </Text>
                            </TouchableOpacity>

                            <View style={{
                                backgroundColor: 'white', borderRadius: 10, width: 170, height: 50, justifyContent: 'center', alignItems: 'center',
                                position: 'absolute', left: 93, top: 210,
                                elevation: 3
                            }}>
                                <Text style={styles.title}>คาร์โบไฮเดรต</Text>
                            </View>
                        </ImageBackground>
                    </View>
                </View>

                <View style={{ margin: 20 }}>
                    <Text style={styles.text}>
                        มีความสำคัญในการให้พลังงานและความอบอุ่นแก่ร่างกาย
                        คุณแม่ควรรับประทานอาหารกลุ่มนี้เท่ากับก่อนตั้งครรภ์เพราะระหว่างตั้งครรภ์กระเพาะอาหารย่อยแป้งและน้ำตาลได้ช้าลงและหากรับประทานมากเกินไป
                        อาจทำให้เกิดภาวะน้ำตาลในเลือดสูงและเกิดภาวะเบาหวานร่วมกับการตั้งครรภ์ได้
                        อาหารกลุ่มนี้ได้แก่ อาหารประเภทแป้งและน้ำตาล เช่น ก๋วยเตี๋ยว ข้าว ขนมหวาน
                      </Text>
                </View>

                <View style={{ margin: 20 }}>
                    <Text style={styles.textbold}>ตัวอย่างอาหาร</Text>
                        <View style={{ margin: 5}}>
                            <Card
                                title="ข้าว"
                                image={require('../../img/food/rice2.jpg')}
                                imageStyle={{ height: 300, margin: 3 }} />
                            <Card
                                title="ก๋วยเตี๋ยว"
                                image={require('../../img/food/noodle.jpg')}
                                imageStyle={{ height: 300, margin: 3 }} />
                            <Card
                                title="ขนมหวาน"
                                image={require('../../img/food/sweet.jpg')}
                                imageStyle={{ height: 300, margin: 3 }} />
                        </View>
                </View>
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    cover: {
        width: '100%',
        height: 237
    },
    text: {
        fontFamily: 'NotoSansThai-Regular',
        fontSize: 16
    },
    textbold:{
        fontFamily: 'NotoSansThai-Bold',
        fontSize: 16
    },
    title: {
        fontFamily: 'NotoSansThai-Regular',
        fontSize: 18
    },
    touchBack: {
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        left: 8,
        top: 10,
        width: 50,
        height: 50,
        borderRadius: 23,
        backgroundColor: 'rgba(247, 247, 247, 0.5)'
    }
})

export default CarboDetail;