import React, { Component } from 'react'
import { View, Text, ScrollView, TouchableOpacity, StyleSheet, Image } from 'react-native'
import { connect } from 'react-redux'

class MainMenu extends Component {

    onPressMenu = (text) => {
        switch (text) {
            case 'FOOD':
                this.props.history.push('/food-menu')
                break;
            case 'EXERCISE':
                this.props.history.push('/exercise-menu')
                break;
            case 'APPOINTMENT' :
                this.props.history.push('/appointment')
                break;
            case 'GROWTH' :
                this.props.history.push('/growth-menu')
                break;
            case 'UNHAPPY' :
                this.props.history.push('/unhappy-menu')
                break;
            case 'DOCTOR' :
                this.props.history.push('/doctor-menu')
                break;
        }
    }

    render() {

        return (
            <ScrollView>
                
                    <Image source={require('../img/main-menu/cover.png')} style={{ height: 93, width: 360 }} />
                
                <TouchableOpacity style={styles.menu} onPress={() => this.onPressMenu('FOOD')}>
                    <Image
                        source={require('../img/main-menu/food.png')}
                        style={{ height: 93, width: 360 }} />
                </TouchableOpacity>
                <TouchableOpacity style={styles.menu} onPress={() => this.onPressMenu('EXERCISE')}>
                    <Image source={require('../img/main-menu/exercise.png')} style={{ height: 93, width: 360 }} />
                </TouchableOpacity>
                <TouchableOpacity style={styles.menu} onPress={() => this.onPressMenu('GROWTH')}>
                    <Image source={require('../img/main-menu/growth.png')} style={{ height: 93, width: 360 }} />
                </TouchableOpacity>
                <TouchableOpacity style={styles.menu} onPress={() => this.onPressMenu('UNHAPPY')}>
                    <Image source={require('../img/main-menu/unhappy.png')} style={{ height: 93, width: 360 }} />
                </TouchableOpacity>
                <TouchableOpacity style={styles.menu} onPress={() => this.onPressMenu('DOCTOR')}>
                    <Image source={require('../img/main-menu/doctor.png')} style={{ height: 93, width: 360 }} />
                </TouchableOpacity>
                <TouchableOpacity style={styles.menu} onPress={() => this.onPressMenu('APPOINTMENT')}>
                    <Image source={require('../img/main-menu/appointment.png')} style={{ height: 93, width: 360 }} />
                </TouchableOpacity>
            </ScrollView>
        )
    }
}
const styles = StyleSheet.create({
    menu: {
        flex: 1,
        backgroundColor: '#EECECE',
        height: 93,
        justifyContent: 'center',
        alignItems: 'center'
    }
})

export default connect(null)(MainMenu)
