import React, { Component } from 'react';
import { View, Text, ScrollView, TouchableOpacity, StyleSheet, Image, TextInput, FlatList, ImageBackground, Button } from 'react-native'
import RNCalendarEvents from 'react-native-calendar-events';

class Appointment extends Component {

    state = {
        title: '',
        detail: '',
        date: '',
        calendars: [],
        events: []
    }

    onBack() {
        console.log('click back')
        this.props.history.push('/main-menu')
    }

    addEvent() {
        this.props.history.push('/add-appointment')
    }

    addDays = (date) => {
        //to get the date of 7 days ahead
        var result = new Date(date);
        result.setDate(result.getDate() + 7);
        console.log('7 days ahead ', result)
        return result;
    }

    UNSAFE_componentWillMount() {
        console.log("calling auth status ")
        RNCalendarEvents.authorizationStatus()
            .then((res) => {
                console.log('auth status', res)
                if (res !== 'authorized') {
                    RNCalendarEvents.authorizeEventStore()
                        .then((res) => {
                            console.log(res)
                            this.forceUpdate()
                            this.fetchCalAndEvents()
                        })
                }
                else {
                    this.fetchCalAndEvents()
                }
            }).catch(err => console.log('error', err))
        console.log('componentWillMount', this.state.events)
    }

    fetchCalAndEvents = () => {
        var date = new Date()
        var daysahead = this.addDays(date)

        //fetch calendars id
        RNCalendarEvents.findCalendars()
            .then((calen) => {
                console.log('fetch calendar', calen)
                this.setState({ calendars: calen })
                console.log('fetch calendars', this.state.calendars.map((ele) => ele.id))

                //fetch incoming events
                RNCalendarEvents.fetchAllEvents(date, daysahead, this.state.calendars.map((ele) => ele.id))
                    .then((res) => {
                        this.setState({ events: res })
                    })
            })
    }

    authCalendar = () => {
        RNCalendarEvents.authorizeEventStore()
            .then((response) => console.log('authCalendar'))
    }

    // fetchAllEvents = () => {
    //     RNCalendarEvents.fetchAllEvents('2019-06-18T12:00:00.000Z', '2019-06-19T12:00:00.000Z')
    //         .then((res) => console.log(res))
    // }

    _keyExtractor = (item, index) => item.id;

    formatter = text => {
        let date = new Date(text)
        return date.toLocaleString('en-GB', { timeZone: 'UTC' })
    }

    render() {
        console.log('render events', this.state.events)
        const options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };
        return (
            <View>
                {this.state.events.length > 0 ? (
                    <ScrollView>
                        <View style={{ zIndex: 1000 }}>
                            <ImageBackground source={require('../../img/main-menu/appointment.png')} style={{ height: 93, width: 360 }}>
                                <TouchableOpacity onPress={() => { this.onBack() }} style={styles.touchBack}>
                                    <Text style={styles.text}> กลับ </Text>
                                </TouchableOpacity>
                            </ImageBackground>
                        </View>
                        <View style={{ margin: 20 }}>
                            <Text style={styles.textbold}>นัดหมายที่ใกล้จะมาถึง</Text>
                            <FlatList
                                style={{ flex: 1 }}
                                data={this.state.events}
                                extraData={this.state}
                                style={{ width: '100%' }}
                                keyExtractor={(item) => item.id}
                                renderItem={({ item }) => {
                                    return (
                                        <View style={{ flexDirection: 'row', width: '100%', marginVertical: 10 }}>
                                            <View style={{
                                                flex: 1,
                                                flexDirection: 'row',
                                                justifyContent: 'flex-start'
                                            }}>
                                                <Text style={styles.title}>
                                                    {item.title}
                                                </Text>
                                            </View>
                                            <View style={{ alignItems: 'flex-end' }}>
                                                <Text style={styles.text}>
                                                    {this.formatter(item.startDate)}
                                                </Text>
                                            </View>
                                        </View>
                                    )
                                }} />
                        </View>
                        <View style={{ justifyContent: 'center', alignItems: 'center', marginBottom: 40 }}>
                            <TouchableOpacity
                                onPress={() => { this.addEvent() }}
                                style={{ backgroundColor: '#FFDA77', width: '50%', height: 40, justifyContent: 'center', alignItems: 'center', borderRadius: 4 }}>
                                <Text style={styles.text}>เพิ่มนัดหมาย</Text>
                            </TouchableOpacity>
                        </View>
                    </ScrollView>
                ) : (
                        <View>
                            <View style={{ zIndex: 1000 }}>
                                <Image source={require('../../img/main-menu/appointment.png')} style={{ height: 93, width: 360 }} />
                            </View>
                            <View style={{ margin: 40, justifyContent: 'center', alignItems: 'center' }}>
                                <Text style={styles.title}>
                                    ยังไม่มีนัดหมาย
                                </Text>

                                <TouchableOpacity
                                    onPress={() => { this.addEvent() }}
                                    style={{ backgroundColor: '#FFDA77', width: '60%', height: 40, justifyContent: 'center', alignItems: 'center', borderRadius: 4, margin: 20 }}>
                                    <Text style={styles.text}>เพิ่มนัดหมาย</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    )}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    backgroundVideo: {
        position: 'absolute',
        top: 0,
        left: 0,
        bottom: 0,
        right: 0,
    },
    text: {
        fontFamily: 'NotoSansThai-Regular',
        fontSize: 15
    },
    textbold: {
        fontFamily: 'NotoSansThai-Bold',
        fontSize: 16
    },
    title: {
        fontFamily: 'NotoSansThai-Regular',
        fontSize: 16
    },
    touchBack: {
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        left: 8,
        top: 10,
        width: 50,
        height: 50,
        borderRadius: 23,
        backgroundColor: 'rgba(247, 247, 247, 0.5)'
    },
    btnBack: {
        width: 40,
        height: 40
    }
})

export default Appointment;