import React, { Component } from 'react';
import { View, Text, ScrollView, TouchableOpacity, StyleSheet, Image, TextInput, FlatList, ImageBackground, Button, DatePickerAndroid } from 'react-native'
import RNCalendarEvents from 'react-native-calendar-events';
import DateTimePicker from "react-native-modal-datetime-picker";

class AddAppointment extends Component {

    state = {
        title: '',
        startDate: '',
        endDate: '',
        isDateTimePickerVisible: false
    }

    onBack() {
        console.log('click back')
        this.props.history.push('/appointment')
    }

    showDateTimePicker = () => {
        this.setState({ isDateTimePickerVisible: true });
    };

    hideDateTimePicker = () => {
        this.setState({ isDateTimePickerVisible: false });
    };

    handleDatePicked = date => {
        console.log("A date has been picked: ", date);
        this.setState({ startDate: date })
        console.log('this.state.startDate ', this.state.startDate)

        const nextDay = this.addDays(date)
        this.setState({ endDate: nextDay })
        this.hideDateTimePicker();
    };

    addDays = (date) => {
        //to get the date of 1 days ahead
        var result = new Date(date);
        result.setDate(result.getDate() + 1);
        console.log('1 days ahead ', result)
        return result;
    }

    saveEvent = () => {
        console.log(this.state.title, this.state.startDate)
        RNCalendarEvents.saveEvent(this.state.title, { startDate: this.state.startDate, endDate: this.state.endDate })
            .then((res) => {
                console.log('save event', res)

            })
            .catch((res) => { console.log(res) })
        this.onBack()
    }

    render() {
        return (
            <View>
                <View style={{ zIndex: 1000 }}>
                    <ImageBackground source={require('../../img/main-menu/appointment.png')} style={{ height: 93, width: 360 }}>
                        <TouchableOpacity onPress={() => { this.onBack() }} style={styles.touchBack}>
                            <Text style={styles.text}> กลับ </Text>
                        </TouchableOpacity>
                    </ImageBackground>
                </View>
                <View style={{ margin: 20 }}>
                    <View style={{ justifyContent: 'center', alignItems: 'center', marginBottom: 20 }}>
                        <Text style={styles.title}>นัดหมายใหม่</Text>
                    </View>

                    <TextInput
                        onChangeText={(text) => {
                            this.setState({ title: text })
                            console.log('title', this.state.title)
                        }}
                        value={this.state.title}
                        style={styles.input}
                        placeholder="ชื่อนัดหมาย" />

                    <View style={{margin: 20}}>
                        <Text style={styles.text}>วัน และเวลาที่นัดหมาย {this.state.startDate == '' ? (
                            <Text style={styles.textFade}>ยังไม่ได้เลือกวัน และเวลา</Text>
                        ) : (<Text />)}
                        </Text>
                    </View>


                    <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                        <TouchableOpacity onPress={this.showDateTimePicker}
                            style={{ backgroundColor: '#FFDA77', width: '70%', height: 40, justifyContent: 'center', alignItems: 'center', borderRadius: 4, margin: 20 }}>
                            <Text style={styles.text}>เลือกวัน และเวลา</Text>
                        </TouchableOpacity>
                        <DateTimePicker
                            isVisible={this.state.isDateTimePickerVisible}
                            onConfirm={this.handleDatePicked}
                            onCancel={this.hideDateTimePicker}
                            mode="datetime"
                        />

                        <TouchableOpacity
                            onPress={() => { this.saveEvent() }}
                            style={{ backgroundColor: '#FFDA77', width: '50%', height: 40, justifyContent: 'center', alignItems: 'center', borderRadius: 4, margin: 20 }}>
                            <Text style={styles.text}>บันทึก</Text>
                        </TouchableOpacity>
                    </View>
                </View>

            </View>
        )
    }
}

const styles = StyleSheet.create({
    text: {
        fontFamily: 'NotoSansThai-Regular',
        fontSize: 15
    },
    textbold: {
        fontFamily: 'NotoSansThai-Bold',
        fontSize: 16
    },
    title: {
        fontFamily: 'NotoSansThai-Regular',
        fontSize: 16
    },
    touchBack: {
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        left: 8,
        top: 10,
        width: 50,
        height: 50,
        borderRadius: 23,
        backgroundColor: 'rgba(247, 247, 247, 0.5)'
    },
    btnBack: {
        width: 40,
        height: 40
    },
    input: {
        height: 40, borderColor: 'gray', borderWidth: 1, borderRadius: 10,
        fontFamily: 'NotoSansThai-Regular'
    },
    textFade: {
        fontFamily: 'NotoSansThai-Regular',
        fontSize: 15,
        color: '#CACACA'
    }
})
export default AddAppointment;