import React, { Component } from 'react'
import { View, Text, ScrollView, TouchableOpacity, StyleSheet, Image, ImageBackground } from 'react-native'

class Swell extends Component {

    onBack = () => {
        this.props.history.push('/unhappy-menu')
    }

    render() {
        return (
            <ScrollView>
                <View style={{ flexDirection: 'row', backgroundColor: '#002C3E', height: 70, alignItems: 'center', justifyContent: 'center' }}>
                    <TouchableOpacity onPress={() => { this.onBack() }}
                        style={styles.touchBack}>
                        <Text style={styles.textWhite}> กลับ </Text>
                    </TouchableOpacity>
                    <View style={{ alignItems: 'center' }}>
                        <Text style={styles.title}>อาการบวม</Text>
                    </View>
                </View>

                <View style={{ margin: 20 }}>
                    <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                        <Image
                            source={require('../../img/unhappy/swell-2.jpg')}
                            style={styles.image} />
                    </View>

                    <View style={{ marginTop: 20 }}>
                        <Text style={styles.text}>
                            <Text style={styles.textbold}>อาการบวม</Text> เกิดจากการยืนหรือนั่งนานๆ อยู่ในท่าทางที่ไม่ถูกต้องหรือสวมเสื้อผ้าคับ
                            {'\n'}
                            {'\n'}
                            <Text style={styles.textbold}>การป้องกันและบรรเทาอาการ</Text>
                            {'\n'}
                            {'\n'}{'    '}1. หลีกเลี่ยงการยืนหรือนั่งท่าเดียวนานๆและการนั่งไขว้ห้าง
                            {'\n'}{'    '}2. นอนยกปลายเท้าให้สูงกว่าสะโพกหรือนอนตะแคงซ้ายช่วยให้เลือดไหลเวียนได้ดี
                            {'\n'}{'    '}3. บริหารกล้ามเนื้อบริเวณขาและข้อเท้าเพื่อช่วยกระตุ้นการไหลเวียนเลือด
                            {'\n'}{'    '}4. สังเกตอาการผิดปกติ ถ้าบวมมากเกินระดับหัวเข่าหรือบวมบริเวณหน้าและลำตัวให้รีบไปพบแพทย์


                        </Text>
                    </View>

                    <View style={{ justifyContent: 'center', alignItems: 'center', marginTop: 10 }}>
                        <Image
                            source={require('../../img/unhappy/swell-1.jpg')}
                            style={styles.image} />
                    </View>

                </View>
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    text: {
        fontFamily: 'NotoSansThai-Regular',
        fontSize: 15
    },
    textbold: {
        fontFamily: 'NotoSansThai-Bold',
        fontSize: 16
    },
    title: {
        fontFamily: 'NotoSansThai-Regular',
        fontSize: 16,
        color: 'white'
    },
    touchBack: {
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        left: 8,
        top: 10,
        width: 50,
        height: 50,
        borderRadius: 23,
        backgroundColor: 'rgba(247, 247, 247, 0.5)'
    },
    textWhite: {
        fontFamily: 'NotoSansThai-Regular',
        fontSize: 15,
        color: 'white'
    },
    image: {
        height: 200,
        width: 250,
        margin: 5,
        borderRadius: 10
    }
})
export default Swell;