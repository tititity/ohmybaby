import React, { Component } from 'react'
import { View, Text, ScrollView, TouchableOpacity, StyleSheet, Image, ImageBackground } from 'react-native'
import { connect } from 'react-redux'

class UnhappyMenu extends Component {

    onPress = (text) => {
        switch (text) {
            case 'SICK':
                this.props.history.push('/sick')
                break;
            case 'WHITE':
                this.props.history.push('/white')
                break;
            case 'PEE':
                this.props.history.push('/pee')
                break;
            case 'STOMACH':
                this.props.history.push('/stomach')
                break;
            case 'BURN':
                this.props.history.push('/burn')
                break;
            case 'BACK':
                this.props.history.push('/unhappy-back')
                break;
            case 'CRAMP':
                this.props.history.push('/cramp')
                break;
            case 'SWELL':
                this.props.history.push('/swell')
                break;
        }
    }

    onBack = () => {
        this.props.history.push('/main-menu')
    }

    render() {
        return (
            <ScrollView>
                <View>
                    <ImageBackground
                        source={require('../../img/main-menu/unhappy.png')} style={{ height: 93, width: 360 }}>
                        <TouchableOpacity style={styles.touchBack} onPress={() => this.onBack()}>
                            <Text style={styles.text}>กลับ</Text>
                        </TouchableOpacity>
                    </ImageBackground>
                </View>

                <View>
                    <TouchableOpacity
                        style={styles.menu}
                        onPress={() => { this.onPress('SICK') }}>
                        <Text style={styles.text}>
                            คลื่นไส้อาเจียน
                        </Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={styles.menu}
                        onPress={() => { this.onPress('WHITE') }}>
                        <Text style={styles.text}>
                            ตกขาว
                        </Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={styles.menu}
                        onPress={() => { this.onPress('PEE') }}>
                        <Text style={styles.text}>
                            ปัสสาวะบ่อย
                        </Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={styles.menu}
                        onPress={() => { this.onPress('STOMACH') }}>
                        <Text style={styles.text}>
                            ท้องผูก
                        </Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={styles.menu}
                        onPress={() => { this.onPress('BURN') }}>
                        <Text style={styles.text}>
                            อาการแสบร้อนยอดอก
                        </Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={styles.menu}
                        onPress={() => { this.onPress('BACK') }}>
                        <Text style={styles.text}>
                            ปวดหลัง
                        </Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={styles.menu}
                        onPress={() => { this.onPress('CRAMP') }}>
                        <Text style={styles.text}>
                            ตะคริว
                        </Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={styles.menu}
                        onPress={() => { this.onPress('SWELL') }}>
                        <Text style={styles.text}>
                            อาการบวม
                        </Text>
                    </TouchableOpacity>
                </View>
            </ScrollView>
        )
    }

}

const styles = StyleSheet.create({
    text: {
        fontFamily: 'NotoSansThai-Regular',
        fontSize: 15,
        color: 'white'
    },
    textbold: {
        fontFamily: 'NotoSansThai-Bold',
        fontSize: 16
    },
    title: {
        fontFamily: 'NotoSansThai-Regular',
        fontSize: 16
    },
    touchBack: {
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        left: 8,
        top: 10,
        width: 50,
        height: 50,
        borderRadius: 23,
        backgroundColor: 'rgba(247, 247, 247, 0.5)'
    },
    menu: {
        height: 70, backgroundColor: '#002C3E', justifyContent: 'center', alignItems: 'center'
    }
})

export default connect(null)(UnhappyMenu);