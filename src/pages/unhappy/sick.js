import React, { Component } from 'react'
import { View, Text, ScrollView, TouchableOpacity, StyleSheet, Image, ImageBackground } from 'react-native'

class Sick extends Component {

    onBack = () => {
        this.props.history.push('/unhappy-menu')
    }

    render() {
        return (
            <ScrollView>
                <View style={{ flexDirection: 'row', backgroundColor: '#002C3E', height: 70, alignItems: 'center', justifyContent: 'center' }}>
                    <TouchableOpacity onPress={() => { this.onBack() }}
                        style={styles.touchBack}>
                        <Text style={styles.textWhite}> กลับ </Text>
                    </TouchableOpacity>
                    <View style={{ alignItems: 'center' }}>
                        <Text style={styles.title}>คลื่นไส้อาเจียน</Text>
                    </View>
                </View>

                <View style={{ margin: 20 }}>
                    <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                        <Image
                            source={require('../../img/unhappy/sick-2.jpg')}
                            style={styles.image} />
                    </View>

                    <View style={{ marginTop: 20 }}>
                        <Text style={styles.text}>
                            <Text style={styles.textbold}>อาการคลื่นไส้อาเจียน</Text> ส่วนใหญ่มักเกิดในตอนเช้า ช่วง 4 เดือนแรกของการตั้งครรภ์ อาจมีอาการอื่นร่วมด้วย เช่น เบื่ออาหาร อ่อนเพลีย น้ำลายมาก ต้องการรับประทานอาหารแปลกๆ
                            {'\n'}
                            {'\n'}
                            <Text style={styles.textbold}>การป้องกันและบรรเทาอาการ</Text>
                            {'\n'}
                            {'\n'}{'    '}1. ควรหลีกเลี่ยงอาหารหรือสิ่งกระตุ้น เช่น อาหารรสจัด อาหารมัน อาหารที่มีกลิ่นฉุน/เหม็น การรับประทานอาหารที่มากเกินไป และการอยู่ในสถานที่ที่มีกลิ่นที่ไม่ชอบ เพื่อลดอาการคลื่นไส้อาเจียน
                            {'\n'}{'    '}2. รับประทานอาหารครั้งละน้อยแต่บ่อยครั้ง รับประทานอาหารแข็งที่ย่อยง่าย เช่น ขนมปังกรอบ
                            {'\n'}{'    '}3. จิบน้ำอุ่นทันทีที่ตื่นนอนแล้วนอนพักสักครู่ (15นาที)
                            {'\n'}{'    '}4. ดื่มน้ำมากๆอาจเป็นน้ำผลไม้หรือน้ำขิงเพื่อป้องกันภาวะขาดน้ำและเกลือแร่
                            {'\n'}{'    '}5. ถ้ามีอาการคลื่นไส้อาเจียนมากตลอดวัน รับประทานอาหารไม่ได้ น้ำหนักลดทำให้ร่างกายขากน้ำเสียสมดุลของน้ำ เกลือแร่ ขาดอาหาร จำเป็นต้องไปพบแพทย์เพื่อนับการรักษา


                        </Text>
                    </View>

                </View>
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    text: {
        fontFamily: 'NotoSansThai-Regular',
        fontSize: 15
    },
    textbold: {
        fontFamily: 'NotoSansThai-Bold',
        fontSize: 16
    },
    title: {
        fontFamily: 'NotoSansThai-Regular',
        fontSize: 16,
        color: 'white'
    },
    touchBack: {
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        left: 8,
        top: 10,
        width: 50,
        height: 50,
        borderRadius: 23,
        backgroundColor: 'rgba(247, 247, 247, 0.5)'
    },
    textWhite: {
        fontFamily: 'NotoSansThai-Regular',
        fontSize: 15,
        color: 'white'
    },
    image: {
        height: 200,
        width: 250,
        margin: 5,
        borderRadius: 10
    }
})
export default Sick;