import React, { Component } from 'react'
import { View, Text, ScrollView, TouchableOpacity, StyleSheet, Image, ImageBackground } from 'react-native'

class UnhappyBack extends Component {

    onBack = () => {
        this.props.history.push('/unhappy-menu')
    }

    render() {
        return (
            <ScrollView>
                <View style={{ flexDirection: 'row', backgroundColor: '#002C3E', height: 70, alignItems: 'center', justifyContent: 'center' }}>
                    <TouchableOpacity onPress={() => { this.onBack() }}
                        style={styles.touchBack}>
                        <Text style={styles.textWhite}> กลับ </Text>
                    </TouchableOpacity>
                    <View style={{ alignItems: 'center' }}>
                        <Text style={styles.title}>ปวดหลัง</Text>
                    </View>
                </View>

                <View style={{ margin: 20 }}>
                    <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                        <Image
                            source={require('../../img/unhappy/back-2.jpg')}
                            style={styles.image} />
                    </View>

                    <View style={{ marginTop: 20 }}>
                        <Text style={styles.text}>
                            <Text style={styles.textbold}>ปวดหลัง</Text> มักพบในไตรมาสที่ 2 และ 3 ของการตั้งครรภ์ซึ่งเกิดจากน้ำหนักตัวที่เพิ่มขึ้นและการถ่วงของครรภ์ที่อยู่ด้านหน้าส่งผลให้กระดูกสันหลังแอ่นเป็นเวลานาน หลังต้องแบกรับน้ำหนักตัวมากขึ้นจนทำให้เกิดอาการปวดหลัง
                            {'\n'}
                            {'\n'}
                            <Text style={styles.textbold}>การป้องกันและบรรเทาอาการ</Text>
                            {'\n'}
                            {'\n'}{'    '}1. วางท่าทางและเปลี่ยนอิริยาบถให้ถูกต้อง เช่น ท่านั่ง ท่ายืน ท่านอน ท่าก้มหรือท่าหยิบของ เพื่อป้องกันอาการเคล็ดหรือปวดหลัง
                            {'\n'}{'    '}2. พักผ่อนให้เพียงพอและหลีกเลี่ยงการอยู่ท่าเดียวนานๆและไม่ควรใช้ที่นอนนุ่มเกินไป เพื่อลดการทำงานของกล้ามเนื้อหลัง
                            {'\n'}{'    '}3. ออกกำลังการและบริหารร่างกายอย่างสม่ำเสมอโดยเฉพาะท่าบริหารกล้ามเนื้อหลัง กล้ามเนื้อหน้าท้อง กล้ามเนื้ออุ้งเชิงกราน และกล้ามเนื้อขา เพื่อเพิ่มความแข็งแรงของกล้ามเนื้อหลังและกล้ามเนื้อหน้าท้องและช่วยลดอาการปวดหลัง
                            {'\n'}{'    '}4. หลีกเลี่ยงการยกของหนักและการสวมรองเท้าส้นสูงเพื่อลดการทำงานของกล้ามเนื้อหลังและน่อง
                            {'\n'}{'    '}5. นวดบริเวณที่ปวดเป็นการกระตุ้นปลายประสาทขนาดใหญ่ เพิ่มการไหลเวียนโลหิต กระตุ้นให้มีการหลั่งสารเอนดอร์ฟิน ส่งผลให้ความรู้สึกปวดลดลงอักทั้งยังเป็นการคลายกล้ามเนื้อลดอาการบวมและอักเสบ
                            {'\n'}{'    '}6. เปลี่ยนท่านอนขณะปวดให้นอนท่ายกสะโพกสูง ในรายที่มีอาการปวดร้าวลงบริเวณต้นขา ถ้าในกรณีปวดร้าวข้างเดียวให้นอนตะแคงด้านตรงข้ามกับข้างที่เจ็บเพื่อเพิ่มการไหลเวียนโลหิต
                            {'\n'}{'    '}7. ประคบด้วยความร้อน ช่วยเพิ่มความยืดหยุ่นของกล้ามเนื้อ ลดการเกร็งตัวของเนื้อเยื่อ


                        </Text>
                    </View>

                    <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                        <Image
                            source={require('../../img/unhappy/back-1.jpg')}
                            style={{margin: 5, borderRadius: 10, height: 300, width: 170}} />
                    </View>

                </View>
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    text: {
        fontFamily: 'NotoSansThai-Regular',
        fontSize: 15
    },
    textbold: {
        fontFamily: 'NotoSansThai-Bold',
        fontSize: 16
    },
    title: {
        fontFamily: 'NotoSansThai-Regular',
        fontSize: 16,
        color: 'white'
    },
    touchBack: {
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        left: 8,
        top: 10,
        width: 50,
        height: 50,
        borderRadius: 23,
        backgroundColor: 'rgba(247, 247, 247, 0.5)'
    },
    textWhite: {
        fontFamily: 'NotoSansThai-Regular',
        fontSize: 15,
        color: 'white'
    },
    image: {
        height: 200,
        width: 250,
        margin: 5,
        borderRadius: 10
    }
})
export default UnhappyBack;