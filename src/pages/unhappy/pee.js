import React, { Component } from 'react'
import { View, Text, ScrollView, TouchableOpacity, StyleSheet, Image, ImageBackground } from 'react-native'

class Pee extends Component {

    onBack = () => {
        this.props.history.push('/unhappy-menu')
    }

    render() {
        return (
            <ScrollView>
                <View style={{ flexDirection: 'row', backgroundColor: '#002C3E', height: 70, alignItems: 'center', justifyContent: 'center' }}>
                    <TouchableOpacity onPress={() => { this.onBack() }}
                        style={styles.touchBack}>
                        <Text style={styles.textWhite}> กลับ </Text>
                    </TouchableOpacity>
                    <View style={{ alignItems: 'center' }}>
                        <Text style={styles.title}>ปัสสาวะบ่อย</Text>
                    </View>
                </View>

                <View style={{ margin: 20 }}>
                    <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                        <Image
                            source={require('../../img/unhappy/pee.jpg')}
                            style={styles.image} />
                    </View>

                    <View style={{ marginTop: 20 }}>
                        <Text style={styles.text}>
                            <Text style={styles.textbold}>ปัสสาวะบ่อย</Text> มักเกิดในไตรมาสแรกและไตรมาสที่ 3 ของการตั้งครรภ์จากมดลูกถูกกดเบียด
                            {'\n'}
                            {'\n'}
                            <Text style={styles.textbold}>การป้องกันและบรรเทาอาการ</Text>
                            {'\n'}
                            {'\n'}{'    '}1. ดื่มน้ำอย่างน้อย 2,000 มิลลิลิตรต่อวันเพื่อช่วยการระบายน้ำปัสสาวะ
                            {'\n'}{'    '}2. ถ่ายปัสสาวะทุก2-3ชั่วโมง ลดปริมาณน้ำดื่มก่อนเข้านอน 2 ชั่วโมง และถ่ายปัสสาวะก่อนเข้านอนเพื่อลดปริมาณน้ำในกระเพาะปัสสาวะ
                            {'\n'}{'    '}3. ไม่ควรกลั้นปัสสาวะเพราะอาจทำให้ระบบทางเดินปัสสาวะอีกเสบติดเชื้อได้



                        </Text>
                    </View>

                </View>
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    text: {
        fontFamily: 'NotoSansThai-Regular',
        fontSize: 15
    },
    textbold: {
        fontFamily: 'NotoSansThai-Bold',
        fontSize: 16
    },
    title: {
        fontFamily: 'NotoSansThai-Regular',
        fontSize: 16,
        color: 'white'
    },
    touchBack: {
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        left: 8,
        top: 10,
        width: 50,
        height: 50,
        borderRadius: 23,
        backgroundColor: 'rgba(247, 247, 247, 0.5)'
    },
    textWhite: {
        fontFamily: 'NotoSansThai-Regular',
        fontSize: 15,
        color: 'white'
    },
    image: {
        height: 200,
        width: 250,
        margin: 5,
        borderRadius: 10
    }
})
export default Pee;