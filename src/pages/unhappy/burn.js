import React, { Component } from 'react'
import { View, Text, ScrollView, TouchableOpacity, StyleSheet, Image, ImageBackground } from 'react-native'

class Burn extends Component {

    onBack = () => {
        this.props.history.push('/unhappy-menu')
    }

    render() {
        return (
            <ScrollView>
                <View style={{ flexDirection: 'row', backgroundColor: '#002C3E', height: 70, alignItems: 'center', justifyContent: 'center' }}>
                    <TouchableOpacity onPress={() => { this.onBack() }}
                        style={styles.touchBack}>
                        <Text style={styles.textWhite}> กลับ </Text>
                    </TouchableOpacity>
                    <View style={{ alignItems: 'center' }}>
                        <Text style={styles.title}>อาการแสบร้อนยอดอก</Text>
                    </View>
                </View>

                <View style={{ margin: 20 }}>
                    <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                        <Image
                            source={require('../../img/unhappy/burn.jpg')}
                            style={styles.image} />
                    </View>

                    <View style={{ marginTop: 20 }}>
                        <Text style={styles.text}>
                            <Text style={styles.textbold}>อาการแสบร้อนยอดอก</Text> มักเป็นที่กลางยอดอก อาจมีอาการจุดเสียดแน่นลิ้นปี่ ถ้าเป็นมากๆอาจกลืนอาหารลำบากได้
                            {'\n'}
                            {'\n'}
                            <Text style={styles.textbold}>การป้องกันและบรรเทาอาการ</Text>
                            {'\n'}
                            {'\n'}{'    '}1. ให้รับประทานอาหารครั้งละน้อยแต่เพิ่มจำนวนมื้ออาหาร เพื่อช่วยให้ย่อยได้ง่ายขึ้น 
                            {'\n'}{'    '}2. หลีกเลี่ยงอาหารที่กระตุ้นให้อาการแสบร้อนยอดอกกำเริบ เช่น อาหารมัน อาหารทอด ช็อคโกแลต และอาหารรสจัด เป็นต้น
                            {'\n'}{'    '}3. ดื่มน้ำ 8-10แก้วต่อวัน แต่ไม่ควรดื่มน้ำร่วมกับการรับประทานอาหารเพราะจะยิ่งทำให้กระเพาะอาหารเต็ม กรดจะดันไหลย้อนกลับได้มากขึ้น
                            {'\n'}{'    '}4. ไม่ควรนอนราบหลังรับประทานอาหาร ควรนั่งหรือนอนในท่าศีรษะสูงเพราะช่วยลดความดันในช่องท้องทำให้กรดไหลย้อนได้ยากขึ้น



                        </Text>
                    </View>

                </View>
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    text: {
        fontFamily: 'NotoSansThai-Regular',
        fontSize: 15
    },
    textbold: {
        fontFamily: 'NotoSansThai-Bold',
        fontSize: 16
    },
    title: {
        fontFamily: 'NotoSansThai-Regular',
        fontSize: 16,
        color: 'white'
    },
    touchBack: {
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        left: 8,
        top: 10,
        width: 50,
        height: 50,
        borderRadius: 23,
        backgroundColor: 'rgba(247, 247, 247, 0.5)'
    },
    textWhite: {
        fontFamily: 'NotoSansThai-Regular',
        fontSize: 15,
        color: 'white'
    },
    image: {
        height: 200,
        width: 250,
        margin: 5,
        borderRadius: 10
    }
})
export default Burn;