import React, { Component } from 'react'
import { View, Text, ScrollView, TouchableOpacity, StyleSheet, Image, ImageBackground } from 'react-native'

class Cramp extends Component {

    onBack = () => {
        this.props.history.push('/unhappy-menu')
    }

    render() {
        return (
            <ScrollView>
                <View style={{ flexDirection: 'row', backgroundColor: '#002C3E', height: 70, alignItems: 'center', justifyContent: 'center' }}>
                    <TouchableOpacity onPress={() => { this.onBack() }}
                        style={styles.touchBack}>
                        <Text style={styles.textWhite}> กลับ </Text>
                    </TouchableOpacity>
                    <View style={{ alignItems: 'center' }}>
                        <Text style={styles.title}>ตะคริว</Text>
                    </View>
                </View>

                <View style={{ margin: 20 }}>
                    <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                        <Image
                            source={require('../../img/unhappy/cramp.jpg')}
                            style={styles.image} />
                    </View>

                    <View style={{ marginTop: 20 }}>
                        <Text style={styles.text}>
                            <Text style={styles.textbold}>ตะคริว</Text> อาจเกิดจากปริมาณแคลเซียมในเลือดต่ำ มดลูกที่ใหญ่ขึ้นกดทับเส้นประสาทบริเวณต้นขาหรือการอยู่ในท่าเดียวนานๆ ทำให้เกิดตะคริวได้ง่าย มักเป็นตะคริวบริเวณขา น่องและแขน
                            {'\n'}
                            {'\n'}
                            <Text style={styles.textbold}>การป้องกันและบรรเทาอาการ</Text>
                            {'\n'}
                            {'\n'}{'    '}1. เมื่อเกิดตะคริวให้เหยียดขาตรง กดหัวเข่าให้ข้อพับแนบกับพื้นและกระดกปลายเท้าขึ้นให้มากที่สุดจนทำให้น่องตึง เพื่อลดการเกร็งของกล้ามเนื้อน่อง
                            {'\n'}{'    '}2. ใช้ความร้อนและความเย็นประคบเพื่อบรรเทาอาการปวด ไม่ควรนวดคลึงเพราะจะทำให้เกิดอาการปวด 
                            {'\n'}{'    '}{'    '}-  ช่วงที่กล้ามเนื้อหดเกร็งมากให้ใช้วิธีประคบด้วยน้ำอุ่นจัดๆ ซึ่งจะช่วยบรรเทาอาการลงได้ 
                            {'\n'}{'    '}{'    '}-  เมื่อมีอาการปวดหรือเจ็บกล้ามเนื้อมาก ใช้{'\n'}การประคบเย็นช่วยบรรเทาอาการได้
                            {'\n'}{'    '}3. หลีกเลี่ยงการ นั่งไขว้ห้างหรือยืนนานๆ
                            {'\n'}{'    '}4. บริหารกล้ามเนื้อขาและข้อเท้าอยู่เสมอ
                            {'\n'}{'    '}5. รับประทานอาหารที่มีแคลเซียมสูง เช่น ไข่แดง ปลาไส้ตัน นม
                            {'\n'}{'    '}6. ถ้าเป็นตะคริวมากหรือบ่อย แพทย์อาจให้วิตามินเสริมแคลเซียมมารับประทาน


                        </Text>
                    </View>

                </View>
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    text: {
        fontFamily: 'NotoSansThai-Regular',
        fontSize: 15
    },
    textbold: {
        fontFamily: 'NotoSansThai-Bold',
        fontSize: 16
    },
    title: {
        fontFamily: 'NotoSansThai-Regular',
        fontSize: 16,
        color: 'white'
    },
    touchBack: {
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        left: 8,
        top: 10,
        width: 50,
        height: 50,
        borderRadius: 23,
        backgroundColor: 'rgba(247, 247, 247, 0.5)'
    },
    textWhite: {
        fontFamily: 'NotoSansThai-Regular',
        fontSize: 15,
        color: 'white'
    },
    image: {
        height: 200,
        width: 250,
        margin: 5,
        borderRadius: 10
    }
})
export default Cramp;