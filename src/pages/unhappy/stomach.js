import React, { Component } from 'react'
import { View, Text, ScrollView, TouchableOpacity, StyleSheet, Image, ImageBackground } from 'react-native'

class Stomach extends Component {

    onBack = () => {
        this.props.history.push('/unhappy-menu')
    }

    render() {
        return (
            <ScrollView>
                <View style={{ flexDirection: 'row', backgroundColor: '#002C3E', height: 70, alignItems: 'center', justifyContent: 'center' }}>
                    <TouchableOpacity onPress={() => { this.onBack() }}
                        style={styles.touchBack}>
                        <Text style={styles.textWhite}> กลับ </Text>
                    </TouchableOpacity>
                    <View style={{ alignItems: 'center' }}>
                        <Text style={styles.title}>อาการท้องผูก</Text>
                    </View>
                </View>

                <View style={{ margin: 20 }}>
                    <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                        <Image
                            source={require('../../img/unhappy/stomach.jpg')}
                            style={styles.image} />
                    </View>

                    <View style={{ marginTop: 20 }}>
                        <Text style={styles.text}>
                            <Text style={styles.textbold}>อาการท้องผูก</Text> เกิดจากลำไส้เคลื่อนไหวช้าลง มีการดูดซึมกลับของน้ำมากขึ้น จึงทำให้อุจจาระจับตัวเป็นก้อน
                            {'\n'}
                            {'\n'}
                            <Text style={styles.textbold}>การป้องกันและบรรเทาอาการ</Text>
                            {'\n'}
                            {'\n'}{'    '}1. ดื่มน้ำอย่างน้อย 6-8 แก้วต่อวัน เพื่อนช่วยให้กากอาหารอ่อนตัวและเคลื่อนผ่านลำไส้ได้สะดวกขึ้น
                            {'\n'}{'    '}2. รับประทานอาหารที่มีกาก เช่น แครอท ฟักทอง เพื่อช่วยให้ระบบการย่อยอาหารทำงานได้ดียิ่งขึ้น
                            {'\n'}{'    '}3. ฝึกนิสัยการขับถ่ายให้เป็นเวลา ไม่กลั้นอุจจาระเพื่อช่วยป้องกันอาการท้องผูก
                            {'\n'}{'    '}4. ออกกำลังกายอย่างสม่ำเสมอเพื่อช่วยให้ลำไส้เคลื่อนไหวได้ดี



                        </Text>
                    </View>

                </View>
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    text: {
        fontFamily: 'NotoSansThai-Regular',
        fontSize: 15
    },
    textbold: {
        fontFamily: 'NotoSansThai-Bold',
        fontSize: 16
    },
    title: {
        fontFamily: 'NotoSansThai-Regular',
        fontSize: 16,
        color: 'white'
    },
    touchBack: {
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        left: 8,
        top: 10,
        width: 50,
        height: 50,
        borderRadius: 23,
        backgroundColor: 'rgba(247, 247, 247, 0.5)'
    },
    textWhite: {
        fontFamily: 'NotoSansThai-Regular',
        fontSize: 15,
        color: 'white'
    },
    image: {
        height: 200,
        width: 250,
        margin: 5,
        borderRadius: 10
    }
})
export default Stomach;