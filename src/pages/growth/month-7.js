import React, { Component } from 'react'
import { View, Text, ScrollView, TouchableOpacity, StyleSheet, Image, ImageBackground } from 'react-native'

class MonthSeven extends Component {

    onBack = () => {
        this.props.history.push('/growth-menu')
    }

    render() {
        return (
            <ScrollView>
                <View style={{ alignItems: 'center', marginBottom: 25 }}>
                    <ImageBackground
                        source={require('../../img/growth/7months.jpg')}
                        style={styles.cover} >

                        <TouchableOpacity
                            onPress={() => { this.onBack() }}
                            style={styles.touchBack}>
                            <Text style={styles.text}>
                                กลับ
                                </Text>
                        </TouchableOpacity>

                        <View style={{
                            backgroundColor: 'white', borderRadius: 10, width: 200, height: 50, justifyContent: 'center', alignItems: 'center',
                            position: 'absolute', left: 85, top: 210,
                            elevation: 3
                        }}>
                            <Text style={styles.title}>เดือนที่ 7 (25 – 28 สัปดาห์)</Text>
                        </View>
                    </ImageBackground>

                    <View style={{ margin: 20, marginTop: 40 }}>
                        <Text style={styles.text}>
                            <Text style={styles.textbold}>เดือนที่ 7 (25 – 28 สัปดาห์)</Text> {'\n'}
                            {'       '}ทารกในครรภ์ลืมตาและหลับตาได้ หายใจได้ มีความยาวประมาณ 37 เซนติเมตร (14.5 นิ้ว)
                            และหนักประมาณ 1,100 กรัม ถ้าทารกเกิดในช่วงนี้อวัยวะต่างๆมีพัฒนาการเพียงพอที่จะให้อยู่รอดได้แต่โอกาสรอดชีวิตอาจมีน้อย{'\n'}
                            {'\n'}{'       '}เมื่อทารกในครรภ์อายุ 7 ขึ้นไป เนื่องจากสมองของลูกน้อยสามารถรับรู้และตอบสนองต่อการมองเห็นได้ในช่วงนี้{'\n'}
                            {'\n'}{'       '}1.การใช้ไฟฉายส่องที่หน้าท้องของคุณแม่ {'\n'}จะช่วยพัฒนาเซลล์ประสาทส่วนรับการมองเห็น{'\n'}ของลูกน้อยให้มีประสิทธิภาพมากขึ้นและการเคลื่อนไฟฉายจะช่วยเสริมสร้างกล้ามเนื้อตาและกล้ามเนื้อบริเวณคอให้มีการพัฒนาและมีความแข็งแรงมากขึ้นสร้างความอยากรู้อยากเห็นให้กับลูกน้อยและฝึกฝนความไวในการเรียนรู้ของลูกน้อย{'\n'}
                        </Text>
                        <View style={{justifyContent:'center', alignItems:'center'}}>
                        <Image
                                source={require('../../img/growth/flashlight.jpg')}
                                style={{ height: 300, width: 300}} />
                        </View>
                            
                    

                        <Text style={styles.text}>
                            {'\n'}{'       '}การส่องไฟควรปฏิบัติอย่างสม่ำเสมอทุกวัน {'\n'}วันละครั้ง ครั้งละ 2-3 นาที โดยทำในขณะที่ลูกตื่นหรือขณะที่ลูกดิ้น ซึ่งเป็นช่วงที่พร้อมสำหรับการมองเห็น ปริมาณแสงที่ไม่สว่างมากจนเกินไปดังนั้นขณะปฏิบัติจึงควรทำในห้องมืดสนิทเพื่อให้ลูกเห็นแสงไฟชัดเจนยิ่งขึ้น การส่องไฟควรปฏิบัติก่อนการฟังดนตรีเพราะหากฟังดนตรีก่อนลูกอาจจะหลับพร้อมกับเสียงดนตรี ทำให้ไม่สามารถตอบสนองต่อการส่องไฟได้

                      </Text>
                    </View>

                </View>
            </ScrollView>
        )
    }

}

const styles = StyleSheet.create({
    cover: {
        width: '100%',
        height: 237
    },
    text: {
        fontFamily: 'NotoSansThai-Regular',
        fontSize: 16
    },
    textbold: {
        fontFamily: 'NotoSansThai-Bold',
        fontSize: 16
    },
    title: {
        fontFamily: 'NotoSansThai-Regular',
        fontSize: 18
    },
    touchBack: {
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        left: 8,
        top: 10,
        width: 50,
        height: 50,
        borderRadius: 23,
        backgroundColor: 'rgba(247, 247, 247, 0.5)'
    }
})

export default MonthSeven;