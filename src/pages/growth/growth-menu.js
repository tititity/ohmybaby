import React, { Component } from 'react'
import { View, Text, ScrollView, TouchableOpacity, StyleSheet, Image, ImageBackground } from 'react-native'
import { connect } from 'react-redux'

class GrowthMenu extends Component {

    onBack = () => {
        this.props.history.push('/main-menu')
    }

    onMenu = (text) => {
        switch (text) {
            case 'M1':
                this.props.history.push('/month1')
                break;
            case 'M2':
                this.props.history.push('/month2')
                break;
            case 'M3':
                this.props.history.push('/month3')
                break;
            case 'M4':
                this.props.history.push('/month4')
                break;
            case 'M5':
                this.props.history.push('/month5')
                break;
            case 'M6':
                this.props.history.push('/month6')
                break;
            case 'M7':
                this.props.history.push('/month7')
                break;
            case 'M8':
                this.props.history.push('/month8')
                break;
            case 'M9':
                this.props.history.push('/month9')
                break;
        }
    }

    render() {
        return (
            <View>
                <ImageBackground
                    source={require('../../img/main-menu/growth.png')} style={{ height: 93, width: 360 }}>
                    <TouchableOpacity style={styles.touchBack} onPress={() => this.onBack()}>
                        <Text style={styles.text}>กลับ</Text>
                    </TouchableOpacity>
                </ImageBackground>

                <View style={{ alignItems: 'center', justifyContent: 'center'}}>
                    <View style={{ flexDirection: 'row', margin: 10 }}>
                        <TouchableOpacity style={{ alignItems: 'center', margin: 10 }}
                            onPress={() => this.onMenu('M1')}>
                            <Image source={require('../../img/growth/month1-menu.png')}
                                style={{ width: 90, height: 90 }} />
                            <Text style={styles.text}>เดือนที่ 1</Text>
                        </TouchableOpacity>

                        <TouchableOpacity style={{ alignItems: 'center' , margin: 10}} onPress={() => this.onMenu('M2')}>
                            <Image source={require('../../img/growth/month2-menu.png')}
                                style={{ width: 90, height: 90 }} />
                            <Text style={styles.text}>เดือนที่ 2</Text>
                        </TouchableOpacity>

                        <TouchableOpacity style={{ alignItems: 'center', margin: 10 }} onPress={() => this.onMenu('M3')}>
                            <Image source={require('../../img/growth/month3-menu.png')}
                                style={{ width: 90, height: 90 }} />
                            <Text style={styles.text}>เดือนที่ 3</Text>
                        </TouchableOpacity>
                    </View>

                    <View style={{ flexDirection: 'row', margin: 10 }}>
                        <TouchableOpacity style={{ alignItems: 'center', margin: 10 }} onPress={() => this.onMenu('M4')}>
                            <Image source={require('../../img/growth/month4-menu.png')}
                                style={{ width: 90, height: 90 }} />
                            <Text style={styles.text}>เดือนที่ 4</Text>
                        </TouchableOpacity>

                        <TouchableOpacity style={{ alignItems: 'center' , margin: 10}} onPress={() => this.onMenu('M5')}>
                            <Image source={require('../../img/growth/month5-menu.png')}
                                style={{ width: 90, height: 90 }} />
                            <Text style={styles.text}>เดือนที่ 5</Text>
                        </TouchableOpacity>

                        <TouchableOpacity style={{ alignItems: 'center' , margin: 10}} onPress={() => this.onMenu('M6')}>
                            <Image source={require('../../img/growth/month6-menu.png')}
                                style={{ width: 90, height: 90 }} />
                            <Text style={styles.text}>เดือนที่ 6</Text>
                        </TouchableOpacity>
                    </View>

                    <View style={{ flexDirection: 'row', margin: 10 }}>
                        <TouchableOpacity style={{ alignItems: 'center', margin: 10 }} onPress={() => this.onMenu('M7')}>
                            <Image source={require('../../img/growth/month7-menu.png')}
                                style={{ width: 90, height: 90 }} />
                            <Text style={styles.text}>เดือนที่ 7</Text>
                        </TouchableOpacity>

                        <TouchableOpacity style={{ alignItems: 'center', margin: 10 }} onPress={() => this.onMenu('M8')}>
                            <Image source={require('../../img/growth/month8-menu.png')}
                                style={{ width: 90, height: 90 }} />
                            <Text style={styles.text}>เดือนที่ 8</Text>
                        </TouchableOpacity>

                        <TouchableOpacity style={{ alignItems: 'center', margin: 10 }} onPress={() => this.onMenu('M9')}>
                            <Image source={require('../../img/growth/month9-menu.png')}
                                style={{ width: 90, height: 90 }} />
                            <Text style={styles.text}>เดือนที่ 9</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    text: {
        fontFamily: 'NotoSansThai-Regular',
        fontSize: 15
    },
    textbold: {
        fontFamily: 'NotoSansThai-Bold',
        fontSize: 16
    },
    title: {
        fontFamily: 'NotoSansThai-Regular',
        fontSize: 16
    },
    touchBack: {
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        left: 8,
        top: 10,
        width: 50,
        height: 50,
        borderRadius: 23,
        backgroundColor: 'rgba(247, 247, 247, 0.5)'
    }
})
export default connect(null)(GrowthMenu)