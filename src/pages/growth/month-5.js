import React, { Component } from 'react'
import { View, Text, ScrollView, TouchableOpacity, StyleSheet, Image, ImageBackground } from 'react-native'

class MonthFive extends Component {

    onBack = () => {
        this.props.history.push('/growth-menu')
    }

    render() {
        return (
            <ScrollView>
                <View style={{ alignItems: 'center', marginBottom: 25 }}>
                    <ImageBackground
                        source={require('../../img/growth/5months.jpg')}
                        style={styles.cover} >

                        <TouchableOpacity
                            onPress={() => { this.onBack() }}
                            style={styles.touchBack}>
                            <Text style={styles.text}>
                                กลับ
                                </Text>
                        </TouchableOpacity>

                        <View style={{
                            backgroundColor: 'white', borderRadius: 10, width: 200, height: 50, justifyContent: 'center', alignItems: 'center',
                            position: 'absolute', left: 85, top: 210,
                            elevation: 3
                        }}>
                            <Text style={styles.title}>เดือนที่ 5 (17 – 20 สัปดาห์)</Text>
                        </View>
                    </ImageBackground>

                    <View style={{ margin: 20, marginTop: 40 }}>
                        <Text style={styles.text}>
                            <Text style={styles.textbold}>เดือนที่ 5 (17 – 20 สัปดาห์)</Text> {'\n'}
                            {'       '}มารดารู้สึกถึงการดิ้นของทารกในครรภ์ ทารกในครรภ์มีพัฒนาการของการนอนหลับ การดูดกลืน การเตะและกำมือได้ มีขนอ่อนและไขปกคลุมผิวหนัง มีเส้นผม ขนคิ้วและขนตา มีความยาวประมาณ 25 เซนติเมตร (10 นิ้ว) และหนักประมาณ 450 กรัม
                            {'\n'}
                            {'\n'} {'       '}เมื่อทารกในครรภ์อายุ 5 เดือนหรือ 20 สัปดาห์ขึ้นไป ระบบประสาทของทารกในครรภ์จะเริ่มตอบสนองต่อการเรียนรู้ได้ดี วิธีที่ช่วยกระตุ้นพัฒนาการของลูกน้อยในครรภ์
                            {'\n'}
                            {'\n'} {'       '}1.การพูดคุยกับลูกน้อยในครรภ์ และการฟังดนตรี การพูดคุยกับลูกน้อยในครรภ์จะทำให้ลูกค้นเคยกับเสียงคุณพ่อและคุณแม่ ช่วยให้มีพัฒนาการทางด้านภาษาและการออกเสียงเร็วกว่าปกติ และยังช่วยส่งเสริมความผูกพันระหว่างคุณแม่และลูก การพูดคุยอาจทักทายลูก เรียกชื่อลูกหรือพูดคุยตามที่ต้องการโดยพูดคุยอย่างเป็นธรรมชาติใช้น้ำเสียงที่นุ่มนวลและอ่อนหวานใช้โทนเสียงสูงหรือพูดประโยคเดิมซ้ำๆ จะทำให้ลูกจำเสียงคุณพ่อคุณแม่ได้เร็วขึ้น คุณพ่อคุณแม่อาจจะร้องเพลงหรือเล่านิทานให้ลูกฟังควรเป็นเพลงเดิมหรือนิทานเรื่องเดิมทุกวันเพราะจะได้คุ้นเคยกับจังหวะของเสียงนิทานได้
                            {'\n'}
                        </Text>
                        <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                            <Image source={require('../../img/growth/talk.jpg')}
                                style={styles.image} />
                        </View>

                        <Text style={styles.text}>
                            {'\n'}{'       '}การฟังดนตรีควรเป็นดนตรีที่คุณแม่ชอบ ฟังแล้วรู้สึกสบาย ผ่อนคลายและควรฟังดนตรีชุดเดิมทุกวัน เพื่อให้ลูกคุ้นเคยกับจังหวะของเสียงดนตรีนั้น ไม่ควรฟังดนตรีประเภทที่มีจังหวะรุนแรง ควรใช้เสียงในระดับปกติ ไม่ควรเปิดเสียงดังมากไป ดังนั้น
                            การพูดคุย การร้องเพลง การเล่านิทาน การฟังดนตรี ควรปฏิบัติอย่างสม่ำเสมอทุกวัน
                            วันละครั้ง ครั้งละ 10 – 15 นาที ควรพูดคุยกับลูกน้อยในช่วงที่ทารกดิ้นซึ่งแสดงว่าทารกตื่นอยู่
                            </Text>
                        <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                            <Image source={require('../../img/growth/music.jpg')}
                                style={styles.image} />
                        </View>
                        <Text style={styles.text}>
                            {'\n'}{'       '}2.การลูบหน้าท้องและการสัมผัสน้ำอุ่นและน้ำเย็น จะช่วยให้ลูกน้อยรู้สึกอบอุ่น มั่นคงและเคยชินกับการสัมผัส ในระยะหลังคลอดถ้าลูกได้รับการสัมผัสเช่นเดียวกับขณะอยู่ในครรภ์ลูกจะหยุดร้องกวนและสงบเงียบเร็วขึ้น ควรปฏิบัติในห้องส่วนตัวที่เงียบสงบ เปิดไฟสลัวและเปิดเพลงเบาๆ เวลาลูบหน้าท้องให้มือสัมผัสกับหน้าท้องอาจใช้แป้งหรือครีมทาผิวชโลมไปด้วยเพื่อไม่ให้เกิดความฝืดควรปฏิบัติสม่ำเสมอทุกวันสามารถทำได้ในทุกช่วงเวลา
                            การสัมผัสโดยใช้น้ำอุ่นและน้ำเย็น เป็นการพัฒนาเซลล์ประสาทรับความรู้สึกร้อนหนาว จะช่วยปรับสภาพลูกน้อยให้เคยชินกับอุณหภูมิภายนอกมดลูกซึ่งเย็นกว่าภายในมดลูกและสามารถปรับตัวเข้าหาสิ่งแวดล้อมใหม่ได้ง่ายและเร็วขึ้น ปฏิบัติได้โดยใช้น้ำเย็นใส่ภาชนะอาจเป็นขวดหรือแก้วให้วางบริเวณแผ่นหลังของลูกซึ่งอยู่ตรงข้ามกับด้านที่ลูกดิ้นสลับกับน้ำอุ่นโดยวางสลับกันขวดละ 2 นาที เมื่อใกล้คลอดให้เพิ่มความเย็นขึ้นทีละน้อยพร้อมกับเพิ่มเวลาการวางเป็น 3 – 5 นาที ควรบอกให้ลูกทราบก่อนที่จะวางน้ำอุ่นและน้ำเย็นลงไปที่หน้าท้องของแม่
                            </Text>
                        <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                            <Image source={require('../../img/growth/touch.jpg')}
                                style={styles.image} />
                        </View>
                        <Text style={styles.text}>
                            {'\n'}{'       '}3.การโยกตัวไปทางด้านหน้าและด้านหลังหรือด้านซ้ายและด้านขวา จะช่วยพัฒนาเซลล์ประสาทส่วนรับการเคลื่อนไหวและการทรงตัวของลูกน้อย และเมื่อคุณแม่โยกตัว หน้า-หลัง หรือ ซ้าย-ขวา วิธีใดวิธีหนึ่งอย่างเป็นระบบจะทำให้ลูกน้อยในครรภ์รู้จักการต้านแรงเพื่อให้ตนเองทรงตัวอยู่ได้ส่งผลให้มีการพัฒนาของกล้ามเนื้อเป็นอย่างดี ลูกน้อยจึงมีความสามารถในการชันคอพลิกตัวไปมาไปเร็วกว่าทารกปกติ จะต้องปฏิบัติซ้ำๆเพื่อให้เกิดความเคยชินเกิดการจดจำและรู้จักตอบสนองต่อสิ่งเร้า ควรทำในสถานที่ที่อากาศถ่ายเทสะดวก เงียบสงบ มีเสียงเพลงเบาๆ ซึ่งจะช่วยให้คุณแม่สงบ ผ่อนคลาย และอารมณ์ดีมากขึ้น
                            </Text>
                        <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                            <Image source={require('../../img/growth/chair.jpg')}
                                style={styles.image} />
                        </View>
                    </View>

                </View>
            </ScrollView >
        )
    }

}

const styles = StyleSheet.create({
    cover: {
        width: '100%',
        height: 237
    },
    text: {
        fontFamily: 'NotoSansThai-Regular',
        fontSize: 16
    },
    textbold: {
        fontFamily: 'NotoSansThai-Bold',
        fontSize: 16
    },
    title: {
        fontFamily: 'NotoSansThai-Regular',
        fontSize: 18
    },
    touchBack: {
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        left: 8,
        top: 10,
        width: 50,
        height: 50,
        borderRadius: 23,
        backgroundColor: 'rgba(247, 247, 247, 0.5)'
    },
    image: {
        height: 200,
        width: 250,
        margin: 5,
        borderRadius: 10
    }
})

export default MonthFive;