import React, { Component } from 'react'
import { View, Text, ScrollView, TouchableOpacity, StyleSheet, Image, ImageBackground } from 'react-native'

class MonthThree extends Component {

    onBack = () => {
        this.props.history.push('/growth-menu')
    }

    render() {
        return (
            <ScrollView>
                <View style={{ alignItems: 'center', marginBottom: 25 }}>
                    <ImageBackground
                        source={require('../../img/growth/3months.jpg')}
                        style={styles.cover} >

                        <TouchableOpacity
                            onPress={() => { this.onBack() }}
                            style={styles.touchBack}>
                            <Text style={styles.text}>
                                กลับ
                                </Text>
                        </TouchableOpacity>

                        <View style={{
                            backgroundColor: 'white', borderRadius: 10, width: 200, height: 50, justifyContent: 'center', alignItems: 'center',
                            position: 'absolute', left: 85, top: 210,
                            elevation: 3
                        }}>
                            <Text style={styles.title}>เดือนที่ 3 (9-12 สัปดาห์)</Text>
                        </View>
                    </ImageBackground>

                    <View style={{ margin: 20, marginTop: 40}}>
                        <Text style={styles.text}>
                            <Text style={styles.textbold}>เดือนที่ 3 (9-12 สัปดาห์)</Text> {'\n'}
                            {'       '}อวัยวะต่างๆ มีการพัฒนาเพิ่มมากขึ้น เริ่มมีการเคลื่อนไหว แต่มารดาอาจจะยังไม่รู้สึกว่าทารกดิ้น ทารกในครรภ์มีความยาวประมาณ 8 เซนติเมตร (3.2 นิ้ว) และหนักประมาณ 45 กรัม
                      </Text>
                    </View>

                </View>
            </ScrollView>
        )
    }

}

const styles = StyleSheet.create({
    cover: {
        width: '100%',
        height: 237
    },
    text: {
        fontFamily: 'NotoSansThai-Regular',
        fontSize: 16
    },
    textbold: {
        fontFamily: 'NotoSansThai-Bold',
        fontSize: 16
    },
    title: {
        fontFamily: 'NotoSansThai-Regular',
        fontSize: 18
    },
    touchBack: {
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        left: 8,
        top: 10,
        width: 50,
        height: 50,
        borderRadius: 23,
        backgroundColor: 'rgba(247, 247, 247, 0.5)'
    }
})

export default MonthThree;