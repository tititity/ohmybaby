import React, { Component } from 'react'
import { database, auth, provider } from '../../firebase'
import { Button } from '@ant-design/react-native'
import { View } from 'react-native'

class Login extends Component {

    state = {
        email: '',
        password: '',
        user: null
    }

    componentDidMount(){
        // const jsonStr = localStorage.getItem(KEY_USER_DATA);
        // const isLoggedIn = jsonStr && JSON.parse(jsonStr).isLoggedIn;

        // if(isLoggedIn){
        //     this.props.history.push('/main-menu')
        // }
    }

    onClickLoginWithFB = () => {
        // auth.signInWithPopup(provider).then(({ user }) => {
        //     this.setState({ user })
        //     console.log('login')
        //     this.props.history.push('/main-menu')
        // })
        console.log('login with facebook')
    }

    render() {
        return (
            <View style={{ alignItems: 'center', justifyContent: 'center'}}>
                <Button type="primary" icon="facebook" onClick={this.onClickLoginWithFB}>Login with Facebook</Button>
            </View>
        )
    }
}

export default Login;