import React, { Component } from 'react'
import { View, Text, ScrollView, TouchableOpacity, StyleSheet, Image } from 'react-native'

class HeadAndNeck extends Component {

    onBack() {
        console.log('click back')
        this.props.history.push('/exercise-menu')
    }

    render() {
        return (
            <View>
                <View style={{ flexDirection: 'row', backgroundColor: '#78BCC4', height: 70, alignItems: 'center', justifyContent: 'center' }}>
                    <TouchableOpacity onPress={() => { this.onBack() }} style={styles.touchBack}>
                        <Text style={styles.text}> กลับ </Text>
                    </TouchableOpacity>
                    <Text style={styles.text}>การบริหารศีรษะและคอ</Text>
                </View>
                <ScrollView>
                    <View style={{ margin: 20 }}>
                        <Text style={styles.text}>
                            <Text style={styles.textbold}>ประโยชน์</Text> ช่วยผ่อนคลายความตึงเครียดของแผ่นหลังและคอ
                        </Text>
                        <View style={{ alignItems: 'center', marginVertical: 10 }}>
                            <Text style={styles.textbold}>
                                ท่าที่ 1
                            </Text>
                        </View>
                        <ScrollView horizontal={true}>
                            <View style={{ flexDirection: 'row' }}>
                                <Image
                                    source={require('../../img/exercise/hn-1-1.jpg')}
                                    style={styles.image} />
                                <Image
                                    source={require('../../img/exercise/hn-1-2.jpg')}
                                    style={styles.image} />
                                <Image
                                    source={require('../../img/exercise/hn-1-3.jpg')}
                                    style={styles.image} />
                            </View>
                        </ScrollView>
                        <View style={{ marginTop: 20 }}>
                            <Text style={styles.text}>
                                1. นั่งขัดสมาธิ หน้าตรง หลังตรง {'\n'}
                                2. หายใจเข้าพร้อมกับก้มหน้าคางชิดอก {'\n'}
                                3. หายใจออกเงยหน้าขึ้นช้าๆ นับเป็น 1 ครั้ง {'\n'}
                                4. ทำทั้งหมด  5  ครั้ง
                            </Text>
                        </View>


                        <View style={{ alignItems: 'center', marginVertical: 10 }}>
                            <Text style={styles.textbold}>
                                ท่าที่ 2
                            </Text>
                        </View>
                        <ScrollView horizontal={true}>
                            <View style={{ flexDirection: 'row' }}>
                                <Image
                                    source={require('../../img/exercise/hn-2-1.jpg')}
                                    style={styles.image} />
                                <Image
                                    source={require('../../img/exercise/hn-2-2.jpg')}
                                    style={styles.image} />
                                <Image
                                    source={require('../../img/exercise/hn-2-3.jpg')}
                                    style={styles.image} />
                            </View>
                        </ScrollView>
                        <View style={{ marginTop: 20 }}>
                            <Text style={styles.text}>
                                1. นั่งขัดสมาธิ หน้าตรง หลังตรง {'\n'}
                                2. หายใจเข้า หันศีรษะไปทางด้านขวาให้ใกล้กับไหล่ขวามากที่สุด {'\n'}
                                3. หายใจออก หน้าตรง  นับเป็น 1 ครั้ง {'\n'}
                                4. ทำทั้งหมด  5  ครั้ง ทำสลับกันทั้ง 2 ด้าน
                            </Text>
                        </View>
                        <View style={{ alignItems: 'center', marginVertical: 10 }}>
                            <Text style={styles.textbold}>
                                ท่าที่ 3
                            </Text>
                        </View>
                        <ScrollView horizontal={true}>
                            <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                                <Image
                                    source={require('../../img/exercise/hn-3-1.jpg')}
                                    style={styles.image} />
                                <Image
                                    source={require('../../img/exercise/hn-3-2.jpg')}
                                    style={styles.image} />
                                <Image
                                    source={require('../../img/exercise/hn-3-3.jpg')}
                                    style={styles.image} />
                            </View>
                        </ScrollView>

                        <View style={{ marginTop: 20 }}>
                            <Text style={styles.text}>
                                1. นั่งขัดสมาธิ หน้าตรง หลังตรง {'\n'}
                                2. หายใจเข้า เอียงศีรษะไปทางด้านขวาให้ใกล้กับไหล่ขวามากที่สุด {'\n'}
                                3. หายใจออก หน้าตรง  นับเป็น 1 ครั้ง {'\n'}
                                4. ทำทั้งหมด 5 ครั้ง ทำสลับกันทั้ง 2 ด้าน {'\n'}
                                {'\n'}
                                {'\n'}
                                {'\n'}
                            </Text>
                        </View>
                    </View>
                </ScrollView>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    image: {
        height: 200,
        width: 250,
        margin: 5
    }, text: {
        fontFamily: 'NotoSansThai-Regular',
        fontSize: 15
    },
    textbold: {
        fontFamily: 'NotoSansThai-Bold',
        fontSize: 16
    },
    title: {
        fontFamily: 'NotoSansThai-Regular',
        fontSize: 16
    },
    touchBack: {
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        left: 8,
        top: 10,
        width: 50,
        height: 50,
        borderRadius: 23,
        backgroundColor: 'rgba(247, 247, 247, 0.5)'
    }
})

export default HeadAndNeck;