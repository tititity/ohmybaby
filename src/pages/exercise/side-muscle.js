import React, { Component } from 'react'
import { View, Text, ScrollView, TouchableOpacity, StyleSheet, Image } from 'react-native'

class SideMuscle extends Component {

    onBack() {
        console.log('click back')
        this.props.history.push('/exercise-menu')
    }

    render() {
        return (
            <View>
                <View style={{ flexDirection: 'row', backgroundColor: '#78BCC4', height: 70, alignItems: 'center', justifyContent: 'center' }}>
                    <TouchableOpacity onPress={() => { this.onBack() }} style={styles.touchBack}>
                        <Text style={styles.text}> กลับ </Text>
                    </TouchableOpacity>
                    <Text style={styles.title}>การบริหารกล้ามเนื้อสีข้าง</Text>
                </View>
                <ScrollView>
                    <View style={{ margin: 20 }}>
                        <Text style={styles.text}>
                            <Text style={styles.textbold}>ประโยชน์</Text> ช่วยลดความตึงตัวของกล้ามเนื้อสีข้าง ช่วยให้หายใจได้สะดวกขึ้น
                        </Text>
                        <View style={{ marginVertical: 10 }}>
                            <Text style={styles.textbold}>
                                ท่าที่ 1
                            </Text>
                        </View>

                        <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                            <Image
                                source={require('../../img/exercise/sm-1-1.jpg')}
                                style={styles.image} />
                        </View>
                        <View style={{ marginTop: 20 }}>
                            <Text style={styles.text}>
                                1. นั่งขัดสมาธิหลังตรง{'\n'}
                                2. หายใจเข้าพร้อมกับยกแขนทั้ง 2 ข้างขึ้นเหนือศีรษะ ฝ่ามือทั้ง 2 ข้างประกบกันไว้สักครู่ ค้างไว้ 5 วินาที {'\n'}
                                3. หายใจออกพร้อมกับเอามือลง นับเป็น 1 ครั้ง  {'\n'}
                                4. ทำทั้งหมด  5  ครั้ง
                            </Text>
                        </View>


                        <View style={{ marginVertical: 10 }}>
                            <Text style={styles.textbold}>
                                ท่าที่ 2
                            </Text>
                        </View>
                        <ScrollView horizontal={true}>
                            <View style={{ flexDirection: 'row' }}>
                                <Image
                                    source={require('../../img/exercise/sm-2-1.jpg')}
                                    style={styles.image} />
                                <Image
                                    source={require('../../img/exercise/sm-2-2.jpg')}
                                    style={styles.image} />
                            </View>
                        </ScrollView>
                        <View style={{ marginTop: 20 }}>
                            <Text style={styles.text}>
                                1. นั่งท่าเดียวกับท่าที่ 1 {'\n'}
                                2. หายใจเข้าพร้อมกับยกแขนขวาเหยียดขึ้นเหนือศีรษะงอข้อศอกและคว่ำมือขวาลง  แล้วเอนตัวไปทางด้านซ้ายให้สีข้างตึง ค้างไว้ 5 วินาที  {'\n'}
                                3. หายใจออกพร้อมเอาแขนลง หน้าตรง นับเป็น 1 ครั้ง{'\n'}
                                4. ทำสลับกันทั้ง 2 ข้าง {'\n'}
                                5. ทำทั้งหมด 5 ครั้ง
                                {'\n'}
                                {'\n'}
                                {'\n'}
                                {'\n'}
                            </Text>
                        </View>

                    </View>
                </ScrollView>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    image: {
        height: 200,
        width: 250,
        margin: 5
    }, text: {
        fontFamily: 'NotoSansThai-Regular',
        fontSize: 15
    },
    textbold: {
        fontFamily: 'NotoSansThai-Bold',
        fontSize: 16
    },
    title: {
        fontFamily: 'NotoSansThai-Regular',
        fontSize: 16
    },
    touchBack: {
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        left: 8,
        top: 10,
        width: 50,
        height: 50,
        borderRadius: 23,
        backgroundColor: 'rgba(247, 247, 247, 0.5)'
    }
})

export default SideMuscle;