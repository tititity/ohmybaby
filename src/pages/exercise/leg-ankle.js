import React, { Component } from 'react'
import { View, Text, ScrollView, TouchableOpacity, StyleSheet, Image } from 'react-native'

class LegAndAnkle extends Component {

    onBack() {
        console.log('click back')
        this.props.history.push('/exercise-menu')
    }

    render() {
        return (
            <View>
                <View style={{ flexDirection: 'row', backgroundColor: '#78BCC4', height: 70, alignItems: 'center', justifyContent: 'center' }}>
                    <TouchableOpacity onPress={() => { this.onBack() }} style={styles.touchBack}>
                        <Text style={styles.text}> กลับ </Text>
                    </TouchableOpacity>
                    <Text style={styles.title}>การบริหารขาและข้อเท้า</Text>
                </View>
                <ScrollView>
                    <View style={{ margin: 20 }}>
                        <Text style={styles.text}>
                            <Text style={styles.textbold}>ประโยชน์</Text> ช่วยให้การไหลเวียนโลหิตไปอวัยวะส่วนปลายดีขึ้น ป้องกันเส้นเลือดขอด
                        </Text>
                        <View style={{ alignItems: 'center', marginVertical: 10 }}>
                            <Text style={styles.textbold}>
                                ท่าที่ 1
                            </Text>
                        </View>

                        <View style={{ flexDirection: 'row' }}>
                            <Image
                                source={require('../../img/exercise/la-2.jpg')}
                                style={styles.image} />
                            <Image
                                source={require('../../img/exercise/la-1.jpg')}
                                style={styles.image} />
                        </View>
                        <View style={{ marginTop: 20 }}>
                            <Text style={styles.text}>
                                1. นั่งพิงผนัง ส้นเท้าชิดกัน วางฝ่ามือราบกับพื้นข้างลำตัว {'\n'}
                                2. หายใจเข้าพร้อมกับกระดกปลายเท้าขึ้น {'\n'}
                                3. หายใจออกพร้อมกับกดข้อเท้าลง นับเป็น 1 ครั้ง{'\n'}
                                4. ทำทั้งหมด  5  ครั้ง
                            </Text>
                        </View>


                        <View style={{ alignItems: 'center', marginVertical: 10 }}>
                            <Text style={styles.textbold}>
                                ท่าที่ 2
                            </Text>
                        </View>
                        <View style={{ flexDirection: 'row' }}>
                            <Image
                                source={require('../../img/exercise/la-4.jpg')}
                                style={styles.image} />
                            <Image
                                source={require('../../img/exercise/la-3.jpg')}
                                style={styles.image} />
                        </View>
                        <View style={{ marginTop: 20 }}>
                            <Text style={styles.text}>
                                1. นั่งพิงผนัง ส้นเท้าชิดกัน วางฝ่ามือราบกับพื้นข้างลำตัว {'\n'}
                                2. หายใจเข้าพร้อมกับบิดข้อเท้าเข้าหาลำตัว {'\n'}
                                3. หายใจออกพร้อมกับบิดข้อเท้าออกนอกลำตัว  นับเป็น 1 ครั้ง {'\n'}
                                4. ทำทั้งหมด  5  ครั้ง
                            </Text>
                        </View>


                        <View style={{ alignItems: 'center' }}>
                            <Text style={{ fontWeight: 'bold', marginVertical: 10 }}>
                                ท่าที่ 3
                            </Text>
                        </View>
                        <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                            <Image
                                source={require('../../img/exercise/la-5.jpg')}
                                style={styles.image} />
                        </View>
                        <View style={{ marginTop: 20 }}>
                            <Text style={styles.text}>
                                1. นั่งพิงผนัง แยกขาทั้ง 2 ข้างให้ห่างจากกันมากที่สุด {'\n'}
                                2. วางเข่าให้ตึง เกร็งปลายเท้าทั้งสองข้างขึ้น ให้นิ้วชี้เข้าหาลำตัวจนน่องตึง {'\n'}
                                3. ทำค้างไว้ 5 วินาที {'\n'}
                                4. ผ่อนปลายเท้าลง  นับเป็น 1 ครั้ง {'\n'}
                                5. ทำทั้งหมด  5  ครั้ง{'\n'}
                                {'\n'}
                                {'\n'}
                                {'\n'}
                            </Text>
                        </View>
                        
                    </View>
                </ScrollView>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    image: {
        height: 200,
        width: 250,
        margin: 5
    }, text: {
        fontFamily: 'NotoSansThai-Regular',
        fontSize: 15
    },
    textbold: {
        fontFamily: 'NotoSansThai-Bold',
        fontSize: 16
    },
    title: {
        fontFamily: 'NotoSansThai-Regular',
        fontSize: 16
    },
    touchBack: {
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        left: 8,
        top: 10,
        width: 50,
        height: 50,
        borderRadius: 23,
        backgroundColor: 'rgba(247, 247, 247, 0.5)'
    }
})

export default LegAndAnkle;