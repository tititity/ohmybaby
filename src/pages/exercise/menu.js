import React, { Component } from 'react'
import { View, Text, ScrollView, TouchableOpacity, StyleSheet, Image, ImageBackground } from 'react-native'
import { Modal } from 'antd-mobile'
import { connect } from 'react-redux'

class ExerciseMenu extends Component {

    state = {
        modalVisible: true
    }

    onClose() {
        this.setState({ modalVisible: false })
    }

    onPress = (text) => {
        switch(text){
            case 'LEG-ANKLE':
                console.log('leg-ankle')
                this.props.history.push('/leg-ankle')
                break;
            case 'HEAD-NECK':
                this.props.history.push('/head-neck')
                break;
            case 'SHOULDER' :
                this.props.history.push('/shoulder')
                break;
            case 'SIDE-MUSCLE' :
                this.props.history.push('/side-muscle')
                break;
            case 'PELVIS' :
                this.props.history.push('/pelvis')
                break;
            case 'BACK' :
                this.props.history.push('/back')
                break;
            case 'HIP' :
                this.props.history.push('/hip')
                break;
            case 'CALF' :
                this.props.history.push('/calf')
                break;
            case 'MUSCLE' :
                this.props.history.push('/muscle')
                break;
            case 'VIDEO' :
                this.props.history.push('/exercise-video')
                break;
            default :
                console.log('default')

        }
    }

    onBack = () => {
        this.props.history.push('/main-menu')
    }

    render() {
        return (
            <View>
                <Modal
                    visible={this.state.modalVisible}>
                    <View style={{ backgroundColor: 'yellow' }}>
                        <Text style={styles.text}>
                            การออกกำลังกายสำหรับหญิงตั้งครรภ์
                            “ การบริหารร่างกายจัดเป็นการออกกำลังกายวิธีหนึ่งที่เหมาะสมสำหรับสตรีมีครรภ์และทารก เพราะการบริหารช่วยให้มีการไหลเวียนเลือดในร่างกายเพิ่มขึ้น ช่วยเพิ่มความสามารถในการทา งานของระบบต่างๆของร่างกาย ทำให้สตรีมีครรภ์ และทารกมีสุขภาพแข็งแรง สตรีมีครรภ์สามารถคลอดได้ง่าย และมีความสุขสบายตลอดระยะตั้งครรภ์ ระยะคลอดและหลังคลอด ”
                            ข้อห้ามในการบริหารร่างกายในระยะตั้งครรภ์
                            ในการบริหารร่างกายอาจมีข้อจำกัดในสตรีมีครรภ์บางราย ได้แก่
                            1.สตรีมีครรภ์ที่มีภาวะเสี่ยงและภาวะแทรกซ้อนในในระยะตั้งครรภ์ปัจจุบัน
                            2.สตรีมีครรภ์ที่เคยมีประวัติการตั้งครรภ์และการคลอดผิดปกติ เช่น การแท้ง มีเลือดออกทางช่องคลอด การคลอดก่อนกำหนด รกลอกตัวก่อนกำหนด รกเกาะต่ำ ถุงน้ำคร่ำรั่วหรือแตกก่อนกำหนด ทารกในครรภ์เจริญเติบโตช้า ครรภ์แฝด ความดันโลหิตสูงในขณะตั้งครรภ์  เป็นต้น
                        </Text>
                        <TouchableOpacity onPress={() => { this.onClose() }}>
                            <Text style={styles.text}>
                                ปิด
                           </Text>
                        </TouchableOpacity>
                    </View>
                </Modal>
                
                {/* menu part */}
                <ScrollView>
                <View>
                    <ImageBackground
                        source={require('../../img/main-menu/exercise.png')} style={{ height: 93, width: 360}}>
                            <TouchableOpacity style={styles.touchBack} onPress={()=>this.onBack()}>
                                <Text>กลับ</Text>
                            </TouchableOpacity>
                    </ImageBackground>
                </View>
                
                    <View>
                        <TouchableOpacity
                            style={{ height: 70, backgroundColor: '#78BCC4', justifyContent: 'center', alignItems: 'center' }}
                            onPress={()=>{this.onPress('LEG-ANKLE')}}>
                                <Text style={styles.text}>
                                    การบริหารขาและข้อเท้า
                                </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            style={{ height: 70, backgroundColor: '#78BCC4', justifyContent: 'center', alignItems: 'center' }}
                            onPress={()=>{this.onPress('HEAD-NECK')}}>
                                <Text style={styles.text}>
                                    การบริหารศรีษะและคอ
                                </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            style={{ height: 70, backgroundColor: '#78BCC4', justifyContent: 'center', alignItems: 'center' }}
                            onPress={()=>{this.onPress('SHOULDER')}}>
                                <Text style={styles.text}>
                                    การบริหารไหล่ หลังส่วนบน หน้าอก และแขน
                                </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            style={{ height: 70, backgroundColor: '#78BCC4', justifyContent: 'center', alignItems: 'center' }}
                            onPress={()=>{this.onPress('SIDE-MUSCLE')}}>
                                <Text style={styles.text}>
                                การบริหารกล้ามเนื้อสีข้าง
                                </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            style={{ height: 70, backgroundColor: '#78BCC4', justifyContent: 'center', alignItems: 'center' }}
                            onPress={()=>{this.onPress('PELVIS')}}>
                                <Text style={styles.text}>
                                การบริหารกล้ามเนื้ออุ้งเชิงกรานและช่องคลอด
                                </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            style={{ height: 70, backgroundColor: '#78BCC4', justifyContent: 'center', alignItems: 'center' }}
                            onPress={()=>{this.onPress('BACK')}}>
                                <Text style={styles.text}>
                                การบริหารกล้ามเนื้อหลัง พื้นเชิงกรานและขา
                                </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            style={{ height: 70, backgroundColor: '#78BCC4', justifyContent: 'center', alignItems: 'center' }}
                            onPress={()=>{this.onPress('HIP')}}>
                                <Text style={styles.text}>
                                การบริหารกล้ามเนื้อหลัง หน้าท้อง สะโพก
                                </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            style={{ height: 70, backgroundColor: '#78BCC4', justifyContent: 'center', alignItems: 'center' }}
                            onPress={()=>{this.onPress('CALF')}}>
                                <Text style={styles.text}>
                                การบริหารกล้ามเนื้อขาและการยืดกล้ามเนื้อน่อง
                                </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            style={{ height: 70, backgroundColor: '#78BCC4', justifyContent: 'center', alignItems: 'center' }}
                            onPress={()=>{this.onPress('MUSCLE')}}>
                                <Text style={styles.text}>
                                การฝึกกล้ามเนื้อ
                                </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            style={{ height: 70, backgroundColor: '#78BCC4', justifyContent: 'center', alignItems: 'center' }}
                            onPress={()=>{this.onPress('VIDEO')}}>
                                <Text style={styles.text}>
                                    วีดีโอสำหรับออกกำลังกาย
                                </Text>
                        </TouchableOpacity>
                    </View>
                </ScrollView>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    text: {
        fontFamily: 'NotoSansThai-Regular',
        fontSize: 15
    },
    textbold: {
        fontFamily: 'NotoSansThai-Bold',
        fontSize: 16
    },
    title: {
        fontFamily: 'NotoSansThai-Regular',
        fontSize: 18
    },
    touchBack: {
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        left: 8,
        top: 10,
        width: 50,
        height: 50,
        borderRadius: 23,
        backgroundColor: 'rgba(247, 247, 247, 0.5)'
    }
})

export default connect(null)(ExerciseMenu);