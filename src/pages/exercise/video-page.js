import React, { Component } from 'react'
import { View, Text, ScrollView, TouchableOpacity, StyleSheet, Image,  Button } from 'react-native'
import Video from 'react-native-af-video-player';


class VideoPage extends Component {

    onBack() {
        console.log('click back')
        this.props.history.push('/exercise-menu')
    }

    videoError(){
        console.log('vid error')
    }

    onBuffer(){
        console.log('on buffer')
    }
    play() {
        this.video.play()
        this.video.seekTo(25)
      }

    render() {
        const url = 'https://www.youtube.com/watch?v=suIdt2_Asjw'
        const logo = require('../../img/growth/icon-100.jpg')
        const title = 'my video'
        const vid = require('../../video/exercise.mp4')
        return (
            <View>
                <View style={{ flexDirection: 'row', backgroundColor: '#78BCC4', height: 70, alignItems: 'center', justifyContent: 'center' }}>
                    <TouchableOpacity onPress={() => { this.onBack() }} style={styles.touchBack}>
                        <Text style={styles.text}> กลับ </Text>
                    </TouchableOpacity>
                    <Text style={styles.title}>วีดีโอสำหรับออกกำลังกาย</Text>
                </View>

                <View style={{margin: 20}}>
                    <Video
                        url={vid}
                        logo={logo}
                        />
                </View>
                   
            </View>
        )
    }
}

const styles = StyleSheet.create({
    backgroundVideo: {
        position: 'absolute',
        top: 0,
        left: 0,
        bottom: 0,
        right: 0,
      },
    text: {
        fontFamily: 'NotoSansThai-Regular',
        fontSize: 15
    },
    textbold: {
        fontFamily: 'NotoSansThai-Bold',
        fontSize: 16
    },
    title: {
        fontFamily: 'NotoSansThai-Regular',
        fontSize: 16
    },
    touchBack: {
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        left: 8,
        top: 10,
        width: 50,
        height: 50,
        borderRadius: 23,
        backgroundColor: 'rgba(247, 247, 247, 0.5)'
    }
})

export default VideoPage;