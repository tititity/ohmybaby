import React, { Component } from 'react'
import { View, Text, ScrollView, TouchableOpacity, StyleSheet, Image } from 'react-native'

class Back extends Component {

    onBack() {
        console.log('click back')
        this.props.history.push('/exercise-menu')
    }

    render() {
        return (
            <View>
                <View style={{ flexDirection: 'row', backgroundColor: '#78BCC4', height: 70, alignItems: 'center', justifyContent: 'center' }}>
                    <TouchableOpacity onPress={() => { this.onBack() }}
                        style={styles.touchBack}>
                        <Text style={styles.text}> กลับ </Text>
                    </TouchableOpacity>
                    <View style={{marginLeft: 40}}>
                        <Text style={styles.title}>การบริหารกล้ามเนื้อหลัง พื้นเชิงกรานและขา</Text>
                    </View>
                </View>
                <ScrollView>
                    <View style={{ margin: 20, marginVertical: 20 }}>
                        <Text style={styles.text}>
                            <Text style={styles.textbold}>ประโยชน์</Text> {'  '}ช่วยให้กล้ามเนื้อต้นขาขยาย 
                            {'\n'}ลดความตึงตัวของกล้ามเนื้อหลังส่วนล่างและตะโพกช่วยยืดขยายข้อต่ออุ้งเชิงกรานช่วยให้คลอดง่าย
                        </Text>
                        <View style={{ alignItems: 'center', marginVertical:10 }}>
                            <Text style={styles.textbold}>
                                ท่าที่ 1
                            </Text>
                        </View>
                        <ScrollView horizontal={true}>
                            <View style={{ flexDirection: 'row' }}>
                                <Image
                                    source={require('../../img/exercise/b-1-1.jpg')}
                                    style={styles.image} />
                                <Image
                                    source={require('../../img/exercise/b-1-2.jpg')}
                                    style={styles.image} />
                                <Image
                                    source={require('../../img/exercise/b-1-3.jpg')}
                                    style={styles.image} />
                            </View>
                        </ScrollView>
                        <View style={{ marginTop: 20 }}>
                            <Text style={styles.text}>
                                {'  '}1. นั่งหลังตรง งอเข่า ประกบฝ่าเท้าทั้ง 2 ข้างเข้าหากัน  {'\n'}
                                {'  '}2. ดึงส้นเท้าให้ชิดบริเวณฝีเย็บให้มากที่สุด {'\n'}
                                {'  '}3. หายใจเข้าพร้อมกับช้อนใต้เข่าทั้งสองขึ้น {'\n'}
                                {'  '}4. หายใจออกพร้อมกดเข่าลงทั้งสองให้ชิดพื้นมากที่สุด {'\n'}{'  '}{'  '}นับเป็น 1 ครั้ง {'\n'}
                                {'  '}5. ทำทั้งหมด  5  ครั้ง
                        </Text>
                        </View>
                    </View>
                </ScrollView>
            </View>
        )
    }

}
const styles = StyleSheet.create({
    image: {
        height: 200,
        width: 250,
        margin: 5
    }, text: {
        fontFamily: 'NotoSansThai-Regular',
        fontSize: 15
    },
    textbold: {
        fontFamily: 'NotoSansThai-Bold',
        fontSize: 16
    },
    title: {
        fontFamily: 'NotoSansThai-Regular',
        fontSize: 16
    },
    touchBack: {
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        left: 8,
        top: 10,
        width: 50,
        height: 50,
        borderRadius: 23,
        backgroundColor: 'rgba(247, 247, 247, 0.5)'
    }
})

export default Back;