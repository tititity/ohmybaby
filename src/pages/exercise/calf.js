import React, { Component } from 'react'
import { View, Text, ScrollView, TouchableOpacity, StyleSheet, Image } from 'react-native'

class Calf extends Component {

    onBack() {
        console.log('click back')
        this.props.history.push('/exercise-menu')
    }

    render() {
        return (
            <View>
                <View style={{ flexDirection: 'row', backgroundColor: '#78BCC4', height: 70, alignItems: 'center', justifyContent: 'center' }}>
                    <TouchableOpacity onPress={() => { this.onBack() }} style={styles.touchBack}>
                        <Text style={styles.text}> กลับ </Text>
                    </TouchableOpacity>
                    <Text style={styles.title}>การบริหารกล้ามเนื้อขาและการยืดกล้ามเนื้อน่อง</Text>
                </View>
                <ScrollView>
                    <View style={{ margin:20}}>
                        <Text style={styles.text}>
                            <Text style={styles.textbold}>ประโยชน์</Text>
                            {"   "}ช่วยขยายขนาดของช่องเชิงกราน {'\n'}ช่วยให้ส่วนของทารกเข้าสู่ช่องเชิงกราน ช่วยในการทรงตัวและท่าทางที่ดีและทำให้กล้ามเนื้อขาและน่องกระชับแข็งแรงการสูบฉีดเลือดจากปลายเท้าและน่องกลับสู่หัวใจให้ดีขึ้นลดอาการบวมและป้องกันเท้าบวม
                        </Text>
                        <View style={{ alignItems: 'center', marginVertical: 10 }}>
                            <Text style={styles.textbold}>
                                ท่าที่  1 การยืดกล้ามเนื้อขา
                            </Text>
                        </View>
                        <ScrollView horizontal={true}>
                            <View style={{ flexDirection: 'row' }}>
                                <Image
                                    source={require('../../img/exercise/c-1-1.jpg')}
                                    style={styles.image} />
                                <Image
                                    source={require('../../img/exercise/c-1-2.jpg')}
                                    style={styles.image} />
                                <Image
                                    source={require('../../img/exercise/c-1-3.jpg')}
                                    style={styles.image} />
                            </View>
                        </ScrollView>
                        <View style={{ marginTop: 20 }}>
                            <Text style={styles.text}>
                                1. ยืนตรงแยกเท้าให้ขนานกันทาบกับฝาผนัง  {'\n'}
                                2. หายใจเข้าแล้วค่อยๆย่อตัวลงนั่งยองๆ ใช้มือทั้ง 2 ข้างค่อยๆดันเข่าให้แยกจากกัน {'\n'}
                                3. หายใจออกพร้อมกับค่อยๆกลับมาอยู่ในท่ายืนตรง นับเป็น 1 ครั้ง {'\n'}
                                4. ทำทั้งหมด 5 ครั้ง
                            </Text>
                        </View>

                        <View style={{ alignItems: 'center', marginVertical: 20 }}>
                            <Text style={styles.textbold}>
                                ท่าที่ 2 การยืดกล้ามเนื้อน่อง
                            </Text>
                            <View style={{ marginTop: 20, marginBottom: 70 }}>
                                <Text style={styles.text}>
                                    ยืนหันหน้าเข้าหากำแพงให้น้ำหนักตัวทิ้งลงไปที่ขาหลัง(ขาที่เหยียด) ทำประมาณ 5 ครั้ง 
                                    เหมาะสำหรับผู้ที่เป็นตะคริวที่ขาและน่อง
                                </Text>
                            </View>
                        </View>
                    </View>
                </ScrollView>
            </View>
        )
    }

}

const styles = StyleSheet.create({
    image: {
        height: 200,
        width: 250,
        margin: 5
    }, text: {
        fontFamily: 'NotoSansThai-Regular',
        fontSize: 15
    },
    textbold: {
        fontFamily: 'NotoSansThai-Bold',
        fontSize: 16
    },
    title: {
        fontFamily: 'NotoSansThai-Regular',
        fontSize: 16
    },
    touchBack: {
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        left: 8,
        top: 10,
        width: 50,
        height: 50,
        borderRadius: 23,
        backgroundColor: 'rgba(247, 247, 247, 0.5)'
    }
})

export default Calf;