import React, { Component } from 'react'
import { View, Text, ScrollView, TouchableOpacity, StyleSheet, Image } from 'react-native'

class Shoulder extends Component {

    onBack() {
        console.log('click back')
        this.props.history.push('/exercise-menu')
    }

    render() {
        return (
            <View>
                <View style={{ flexDirection: 'row', backgroundColor: '#78BCC4', height: 70, alignItems: 'center', justifyContent: 'center' }}>
                    <TouchableOpacity onPress={() => { this.onBack() }} style={styles.touchBack}>
                        <Text style={styles.text}> กลับ </Text>
                    </TouchableOpacity>
                    <View style={{marginLeft:40}}>
                        <Text style={styles.title}>การบริหารไหล่ หลังส่วนบน หน้าอก และแขน</Text>
                    </View>
                    
                </View>
                <ScrollView>
                    <View style={{ margin: 20 }}>
                        <Text style={styles.text}>
                            <Text style={styles.textbold}>ประโยชน์</Text> เพื่อป้องกันอาการปวดหลังส่วนบนและขา เพื่อเพิ่มความจุของปอดและกล้ามเนื้อพยุงเต้านม
                        </Text>
                        <View style={{marginVertical: 10 }}>
                            <Text style={styles.textbold}>
                                ท่าที่ 1
                            </Text>
                        </View>
                        <ScrollView horizontal={true}>
                            <View style={{ flexDirection: 'row' }}>
                                <Image
                                    source={require('../../img/exercise/s-1-1.jpg')}
                                    style={stylex.image} />
                            </View>
                        </ScrollView>
                        <View style={{ marginTop: 20 }}>
                            <Text style={styles.text}>
                                1. นั่ง ศีรษะ คอและหลังตรง ปล่อยแขนทั้งสองข้างวางข้างสะโพก {'\n'}
                                2. หายใจเข้าพร้อมกับยกไหล่ทั้ง 2 ข้างให้สูงขึ้น {'\n'}
                                3. หายใจออกปล่อยไหล่ลง นับเป็น 1 ครั้ง  {'\n'}
                                4. ทำทั้งหมด  5  ครั้ง
                            </Text>
                        </View>


                        <View style={{marginVertical: 10 }}>
                            <Text style={styles.textbold}>
                                ท่าที่ 2
                            </Text>
                        </View>
                        <ScrollView horizontal={true}>
                            <View style={{ flexDirection: 'row' }}>
                                <Image
                                    source={require('../../img/exercise/s-2-1.jpg')}
                                    style={styles.image} />
                                <Image
                                    source={require('../../img/exercise/s-2-2.jpg')}
                                    style={styles.image}/>
                            </View>
                        </ScrollView>
                        <View style={{ marginTop: 20 }}>
                            <Text style={styles.text}>
                                1. นั่ง ศีรษะ คอและหลังตรง ปล่อยแขนทั้งสองข้างวางข้างสะโพก {'\n'}
                                2. ยกมือจับไหล่ทั้ง 2 ข้าง หายใจเข้า-ออกช้าๆพร้อมเอามือจับไหล่ วาดข้อศอกเป็นวงกลม นับเป็น 1 ครั้ง {'\n'}
                                3. ทำทั้งหมด 5 ครั้ง
                            </Text>
                        </View>

                        <View style={{marginVertical: 10 }}>
                            <Text style={styles.textbold}>
                                ท่าที่ 3
                            </Text>
                        </View>
                        <ScrollView horizontal={true}>
                            <View style={{ flexDirection: 'row' }}>
                                <Image
                                    source={require('../../img/exercise/s-3-1.jpg')}
                                    style={styles.image} />
                                <Image
                                    source={require('../../img/exercise/s-3-2.jpg')}
                                    style={styles.image} />
                            </View>
                        </ScrollView>
                        <View style={{ marginTop: 20 }}>
                            <Text style={styles.text}>
                                1. ยืนตรง เท้าทั้งสองข้างห่างกันพอควร {'\n'}
                                2. ประสานมือทั้ง 2 ข้างด้านหน้า {'\n'}
                                3. ค่อยๆพลิกมือพร้อมกับยกมือขึ้นเหนือศีรษะ {'\n'}
                                4. หายใจเข้าลึกๆ ค้างไว้ 3 วินาที {'\n'}
                                5. ค่อยๆลดมือลงพร้อมหายใจออกช้าๆ ทำทั้งหมด 5 ครั้ง
                                {'\n'}
                                {'\n'}
                                {'\n'}
                            </Text>
                        </View>
                        
                    </View>
                </ScrollView>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    image: {
        height: 200,
        width: 250,
        margin: 5
    }, text: {
        fontFamily: 'NotoSansThai-Regular',
        fontSize: 15
    },
    textbold: {
        fontFamily: 'NotoSansThai-Bold',
        fontSize: 16
    },
    title: {
        fontFamily: 'NotoSansThai-Regular',
        fontSize: 16
    },
    touchBack: {
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        left: 8,
        top: 10,
        width: 50,
        height: 50,
        borderRadius: 23,
        backgroundColor: 'rgba(247, 247, 247, 0.5)'
    }
})

export default Shoulder;