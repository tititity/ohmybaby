import React, { Component } from 'react'
import { View, Text, ScrollView, TouchableOpacity, StyleSheet, Image } from 'react-native'

class Muscle extends Component {
    onBack() {
        console.log('click back')
        this.props.history.push('/exercise-menu')
    }

    render() {
        return (
            <View>
                <View style={{ flexDirection: 'row', backgroundColor: '#78BCC4', height: 70, alignItems: 'center', justifyContent: 'center' }}>
                    <TouchableOpacity onPress={() => { this.onBack() }} style={styles.touchBack}>
                        <Text style={styles.text}> กลับ </Text>
                    </TouchableOpacity>
                    <Text style={styles.title}>การฝึกกล้ามเนื้อ</Text>
                </View>
                <ScrollView>
                    <View style={{ margin: 20 }}>
                        <Text style={styles.text}>
                            <Text style={styles.textbold}>ประโยชน์</Text> เป็นท่าที่ยืดขยายกล้ามเนื้ออุ้งเชิงกรานและกล้ามเนื้อฝีเย็บ ท่านี้เป็นท่าหนึ่งที่ใช้คลอดได้
                        </Text>
                        
                        <View style={{ marginVertical: 10}}>
                            <Text style={styles.textbold}>
                                ท่าที่ 1
                            </Text>
                        </View>
                        
                        <ScrollView horizontal={true}>
                            <View style={{ flexDirection: 'row' }}>
                                <Image
                                    source={require('../../img/exercise/m-1-1.jpg')}
                                    style={{ height: 80, width: 150, margin: 5 }} />
                            </View>
                        </ScrollView>
                        <View style={{ marginTop: 20 }}>
                            <Text style={styles.text}>
                                1. นั่งยองๆ เข่าแยกจากกันฝ่าเท้าวางราบกับพื้น
                            </Text>
                        </View>

                        <View style={{ marginTop: 20 }}>
                            <Text style={styles.text}>
                                <Text style={styles.textbold}>คำแนะนำในการบริหารร่างกาย</Text>{'\n'}
                                1. การบริหารร่างกายทุกท่าคุณแม่ควรทำ 5 – 10 ครั้งต่อ 1 ท่า{'\n'}
                                2. ควรบริหารร่างกายอย่างน้อยครั้งละ 30 นาที 3 ครั้งต่อสัปดาห์{'\n'}
                            </Text>
                        </View>

                    </View>
                </ScrollView>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    text: {
        fontFamily: 'NotoSansThai-Regular',
        fontSize: 15
    },
    textbold: {
        fontFamily: 'NotoSansThai-Bold',
        fontSize: 16
    },
    title: {
        fontFamily: 'NotoSansThai-Regular',
        fontSize: 16
    },
    touchBack: {
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        left: 8,
        top: 10,
        width: 50,
        height: 50,
        borderRadius: 23,
        backgroundColor: 'rgba(247, 247, 247, 0.5)'
    }
})

export default Muscle;