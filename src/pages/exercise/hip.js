import React, { Component } from 'react'
import { View, Text, ScrollView, TouchableOpacity, StyleSheet, Image } from 'react-native'

class Hip extends Component {

    onBack() {
        console.log('click back')
        this.props.history.push('/exercise-menu')
    }

    render() {
        return (
            <View>
                <View style={{ flexDirection: 'row', backgroundColor: '#78BCC4', height: 70, alignItems: 'center', justifyContent: 'center' }}>
                    <TouchableOpacity onPress={() => { this.onBack() }} style={styles.touchBack}>
                        <Text style={styles.text}> กลับ </Text>
                    </TouchableOpacity>
                    <Text style={styles.title}>การบริหารกล้ามเนื้อหลัง หน้าท้อง สะโพก</Text>
                </View>
                <ScrollView>
                    <View style={{ margin: 20 }}>
                        <Text style={styles.text}>
                            <Text style={styles.textbold}>ประโยชน์</Text> ป้องกันและบรรเทาอาการปวดหลัง  เพิ่มความแข็งแรงของต้นขา เข่าและข้อเท้าช่วยให้การทรงตัวดี
                        </Text>
                        <View style={{ alignItems: 'center', marginVertical: 10 }}>
                            <Text style={styles.textbold}>
                                ท่าที่ 1 นั่งแบบญี่ปุ่น
                            </Text>
                        </View>
                        <ScrollView horizontal={true}>
                            <View style={{ flexDirection: 'row' }}>
                                <Image
                                    source={require('../../img/exercise/h-1-1.jpg')}
                                    style={styles.image} />
                                <Image
                                    source={require('../../img/exercise/h-1-2.jpg')}
                                    style={styles.image} />
                            </View>
                        </ScrollView>
                        <View style={{ marginTop: 20 }}>
                            <Text style={styles.text}>
                                1. นั่งคุกเข่า ให้ส่วนของก้นวางบนส้นเท้า {'\n'}
                                2. ปลายนิ้วเท้าชิด เข่าทั้งสองห่างกัน {'\n'}
                                3. หายใจเข้าพร้อมกับค่อยๆโน้มลำตัวไปข้างหน้าโดยใช้ข้อศอกรับน้ำหนักและมือทั้ง 2 ข้างวางกับพื้น {'\n'}
                                4. ค่อยๆโน้มตัวไปข้างหน้าให้ไกลมากที่สุด หน้าผากจรดพื้น{'\n'}
                                5. แขนเหยียดตรง วางไว้เหนือศีรษะ{'\n'}
                                6. ขณะทำ ให้หลังเป็นเส้นตรงตั้งแต่ศีรษะถึงสะโพกพร้อมหายใจเข้า-ออกยาวๆ นับเป็น 1 ครั้ง {'\n'}
                                7. ทำทั้งหมด 5 ครั้ง
                            </Text>
                        </View>

                        <View style={{ alignItems: 'center', marginVertical: 10 }}>
                            <Text style={styles.textbold}>
                                ท่าที่ 2 ท่าแมว
                            </Text>
                        </View>
                        <ScrollView horizontal={true}>
                            <View style={{ flexDirection: 'row' }}>
                                <Image
                                    source={require('../../img/exercise/h-2-1.jpg')}
                                    style={styles.image} />
                                <Image
                                    source={require('../../img/exercise/h-2-2.jpg')}
                                    style={styles.image} />
                            </View>
                        </ScrollView>
                        <View style={{ marginTop: 20 }}>
                            <Text style={styles.text}>
                                1. อยู่ในท่าคลาน หลังตรงขนานกับพื้น แขนเหยียดตรง {'\n'}
                                2. หายใจเข้าพร้อมกับโก่งหลังขึ้น ก้มศีรษะลงให้คางชิดอก {'\n'}
                                3. ผ่อนลมหายใจออก พร้อมกับเงยหน้าขึ้นช้าๆ นับเป็น 1 ครั้ง {'\n'}
                                4. ทำทั้งหมด 5 ครั้ง
                            </Text>
                        </View>

                    </View>
                </ScrollView>
            </View>
        )
    }

}
const styles = StyleSheet.create({
    image: {
        height: 200,
        width: 250,
        margin: 5
    }, text: {
        fontFamily: 'NotoSansThai-Regular',
        fontSize: 15
    },
    textbold: {
        fontFamily: 'NotoSansThai-Bold',
        fontSize: 16
    },
    title: {
        fontFamily: 'NotoSansThai-Regular',
        fontSize: 16
    },
    touchBack: {
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        left: 8,
        top: 10,
        width: 50,
        height: 50,
        borderRadius: 23,
        backgroundColor: 'rgba(247, 247, 247, 0.5)'
    }
})

export default Hip;