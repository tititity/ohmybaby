import React, { Component } from 'react'
import { View, Text, ScrollView, TouchableOpacity, StyleSheet, Image } from 'react-native'

class Pelvis extends Component {

    onBack() {
        console.log('click back')
        this.props.history.push('/exercise-menu')
    }

    render() {
        return (
            <View>
                <View style={{ flexDirection: 'row', backgroundColor: '#78BCC4', height: 70, alignItems: 'center', justifyContent: 'center' }}>
                    <TouchableOpacity onPress={() => { this.onBack() }} style={styles.touchBack}>
                        <Text style={styles.text}> กลับ </Text>
                    </TouchableOpacity>
                    <View style={{marginLeft:40}}>
                    <Text style={styles.title}>การบริหารกล้ามเนื้ออุ้งเชิงกรานและช่องคลอด</Text>
                    </View>
                    
                </View>
                <ScrollView>
                    <View style={{ margin: 20 }}>
                        <Text style={styles.text}>
                            <Text style={styles.textbold}>ประโยชน์</Text> ช่วยให้กล้ามเนื้อพื้นเชิงกราน ช่องคลอดและฝีเย็บมีความยืดหยุ่น และช่วยให้คลอดง่ายขึ้น
                        </Text>
                        <View style={{marginVertical: 10 }}>
                            <Text style={styles.textbold}>
                                ท่าที่ 1
                            </Text>
                        </View>
                        
                        <ScrollView horizontal={true}>
                            <View style={{ flexDirection: 'row' }}>
                                <Image
                                    source={require('../../img/exercise/pv-1.jpg')}
                                    style={{ height: 80, width: 150, margin: 5 }} />
                            </View>
                        </ScrollView>

                        <View style={{ marginTop: 20 }}>
                            <Text style={styles.text}>
                                1. เกร็งหรือขมิบกล้ามเนื้อฝีเย็บพร้อมสูดลมหายใจเข้า {'\n'}
                                2. สลับกับคลายกล้ามเนื้อรอบๆฝีเย็บ พร้อมกับผ่อนลมหายใจ {'\n'}
                                3. เกร็งและคลายกล้ามเนื้อเป็นจังหวะ {'\n'}
                                4. ทำประมาณ 50 ครั้งต่อวัน
                            </Text>
                        </View>
                        
                    </View>
                </ScrollView>      
            </View>
        )
    }
}

const styles = StyleSheet.create({
    text: {
        fontFamily: 'NotoSansThai-Regular',
        fontSize: 15
    },
    textbold: {
        fontFamily: 'NotoSansThai-Bold',
        fontSize: 16
    },
    title: {
        fontFamily: 'NotoSansThai-Regular',
        fontSize: 15
    },
    touchBack: {
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        left: 8,
        top: 10,
        width: 50,
        height: 50,
        borderRadius: 23,
        backgroundColor: 'rgba(247, 247, 247, 0.5)'
    }
})
export default Pelvis;