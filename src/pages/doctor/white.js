import React, { Component } from 'react'
import { View, Text, ScrollView, TouchableOpacity, StyleSheet, Image, ImageBackground } from 'react-native'

class WhiteDoctor extends Component {

    onBack = () => {
        this.props.history.push('/doctor-menu')
    }

    render() {
        return (
            <ScrollView>
                <View style={{ flexDirection: 'row', backgroundColor: '#EECECF', height: 70, alignItems: 'center', justifyContent: 'center' }}>
                    <TouchableOpacity onPress={() => { this.onBack() }}
                        style={styles.touchBack}>
                        <Text style={styles.text}> กลับ </Text>
                    </TouchableOpacity>
                    <View style={{ alignItems: 'center' }}>
                        <Text style={styles.title}>อาการตกขาว-คัน</Text>
                    </View>
                </View>

                <View style={{ margin: 20 }}>
                    <Text style={styles.text}>
                        <Text style={styles.textbold}>ลักษณะตกขาวปกติ</Text>
                        {'\n'}
                        {'    '}จะมีลักษณะเป็นมูกใส หรือขาวขุ่นคล้ายแป้งเปียก ปริมาณไม่มากอาจพบได้ทุกวัน เล็กน้อย
                        {'\n'}
                    </Text>
                    <View style={{ justifyContent: 'center', alignItems: 'center', margin: 20 }}>
                        <Image
                            source={require('../../img/unhappy/white.jpg')}
                            style={styles.image} />
                    </View>
                    <Text style={styles.text}>
                        <Text style={styles.textbold}>ลักษณะตกขาวผิดปกติ</Text>
                        {'\n'}
                        {'\n'}{'    '}ตกขาวที่เกิดจากการติดเชื้อรามีลักษณะคล้ายนมบูด
                        </Text>
                    <View style={{ justifyContent: 'center', alignItems: 'center', margin: 20 }}>
                        <Image
                            source={require('../../img/unhappy/fungi.jpg')}
                            style={styles.image} />
                    </View>
                    <Text style={styles.text}>
                        {'\n'}{'    '}ตกขาวที่เกิดจากการติดเชื้อแบคทีเรียมีสีเหลืองแกมเขียวกลิ่นคาวปลา แสบคัน
                        </Text>
                    <View style={{ justifyContent: 'center', alignItems: 'center', margin: 20 }}>
                        <Image
                            source={require('../../img/unhappy/bacteria.jpg')}
                            style={styles.image} />
                    </View>
                    <Text style={styles.text}>
                        {'\n'}{'    '}ตกขาวที่เกิดจากการติดเชื้อพยาธิมีลักษณะสีขาว, เหลือง เป็นฟอง มีปริมาณมาก และคันมาก
                        </Text>
                    <View style={{ justifyContent: 'center', alignItems: 'center', margin: 20 }}>
                        <Image
                            source={require('../../img/unhappy/para.jpg')}
                            style={styles.image} />
                    </View>
                </View>
            </ScrollView >
        )
    }
}

const styles = StyleSheet.create({
    text: {
        fontFamily: 'NotoSansThai-Regular',
        fontSize: 15
    },
    textbold: {
        fontFamily: 'NotoSansThai-Bold',
        fontSize: 16
    },
    title: {
        fontFamily: 'NotoSansThai-Regular',
        fontSize: 16
    },
    touchBack: {
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        left: 8,
        top: 10,
        width: 50,
        height: 50,
        borderRadius: 23,
        backgroundColor: 'rgba(247, 247, 247, 0.5)'
    },
    textWhite: {
        fontFamily: 'NotoSansThai-Regular',
        fontSize: 15,
        color: 'white'
    },
    image: {
        height: 200,
        width: 250,
        margin: 5,
        borderRadius: 10
    }
})
export default WhiteDoctor;