import React, { Component } from 'react'
import { View, Text, ScrollView, TouchableOpacity, StyleSheet, Image, ImageBackground } from 'react-native'

class Pain extends Component {

    onBack = () => {
        this.props.history.push('/doctor-menu')
    }

    render() {
        return (
            <ScrollView>
                <View style={{ flexDirection: 'row', backgroundColor: '#EECECF', height: 70, alignItems: 'center', justifyContent: 'center' }}>
                    <TouchableOpacity onPress={() => { this.onBack() }}
                        style={styles.touchBack}>
                        <Text style={styles.text}> กลับ </Text>
                    </TouchableOpacity>
                    <View style={{ alignItems: 'center' }}>
                        <Text style={styles.title}>เจ็บครรภ์คลอดก่อนกำหนด</Text>
                    </View>
                </View>

                <View style={{ margin: 20 }}>
                    <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                        <Image
                            source={require('../../img/unhappy/pain-1.jpg')}
                            style={styles.image} />
                    </View>
                </View>

                <View style={{margin: 20}}>
                    <Text style={styles.text}>
                        เป็นอาการที่ กล้ามเนื้อมดลูกหดรัดตัวอย่างสม่ำเสมอ 
                        โดยหดรัดตัวแรงและถี่ขึ้นเรื่อย ๆ ทุก 10 นาที (6 ครั้ง ใน 1 ชั่วโมง) 
                        โดยจะมีอาการปวดท้องเป็นพัก ๆ พร้อมกับเวลาที่มดลูกหดตัว นอนพักแล้วไม่หายปวด 
                        ซึ่งอาการปวดท้องนี้จะคล้าย ๆ กับเวลาปวดประจำเดือน ทั้งที่ขณะนั้นคุณแม่ไม่ได้มีอาการท้องเสียหรืออาหารไม่ย่อยแต่อย่างใด 
                        มีอาการปวดหลังชนิดที่ร้าวลงไปถึงด้านล่างบริเวณก้นกบร่วมกับการปวดท้อง 
                        ปวดถ่วงหรือปวดหน่วง ๆ ในอุ้งเชิงกราน และอาจจะร้าวไปที่ต้นขา 
                        โดยที่รับประทานยาแก้ปวด เช่น พาราเซตามอล (Paracetamol) แล้วอาการไม่ดีขึ้น 
                        มีมูกเลือดออกทางช่องคลอด และมีน้ำไหลออกมาจากช่องคลอด 
                        (ลักษณะไหลโชกออกมาทางช่องคลอดคล้ายกับปัสสาวะราดและกลั้นไม่อยู่) 
                        หรือมีระดูขาวออกมา บางทีอาจมีมูกปนเลือดออกมาด้วย

                    </Text>
                </View>
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    text: {
        fontFamily: 'NotoSansThai-Regular',
        fontSize: 15
    },
    textbold: {
        fontFamily: 'NotoSansThai-Bold',
        fontSize: 16
    },
    title: {
        fontFamily: 'NotoSansThai-Regular',
        fontSize: 16
    },
    touchBack: {
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        left: 8,
        top: 10,
        width: 50,
        height: 50,
        borderRadius: 23,
        backgroundColor: 'rgba(247, 247, 247, 0.5)'
    },
    textWhite: {
        fontFamily: 'NotoSansThai-Regular',
        fontSize: 15,
        color: 'white'
    },
    image: {
        height: 200,
        width: 250,
        margin: 5,
        borderRadius: 10
    }
})

export default Pain