import React, { Component } from 'react'
import { View, Text, ScrollView, TouchableOpacity, StyleSheet, Image, ImageBackground } from 'react-native'

class Baby extends Component {

    onBack = () => {
        this.props.history.push('/doctor-menu')
    }

    render() {
        return (
            <ScrollView>
                <View style={{ flexDirection: 'row', backgroundColor: '#EECECF', height: 70, alignItems: 'center', justifyContent: 'center' }}>
                    <TouchableOpacity onPress={() => { this.onBack() }}
                        style={styles.touchBack}>
                        <Text style={styles.text}> กลับ </Text>
                    </TouchableOpacity>
                    <View style={{ alignItems: 'center' }}>
                        <Text style={styles.title}>ลูกดิ้นลดลงหรือไม่ดิ้น</Text>
                    </View>
                </View>

                <View style={{ margin: 20 }}>
                    <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                        <Image
                            source={require('../../img/unhappy/blood-1.jpg')}
                            style={styles.image} />
                    </View>
                </View>

                <View style={{margin: 20}}>
                    <Text style={styles.text}>
                        {'     '}การดิ้นของทารกในครรภ์มีความสำคัญมากเนื่องจากแสดงถึงการมีชีวิตอยู่ของทารกและบ่งบอกถึงสุขภาพของทารก 
                        การดิ้นของทารกจะสม่ำเสมอและค่อนข้างคงที่อาจรู้สึกถึง การถีบ การเตะ กระทุ้ง หมุนตัว และโก่งตัว
                        {'\n'}
                        {'\n'}
                    </Text>
                </View>

                <View style={{ margin: 20 }}>
                    <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                        <Image
                            source={require('../../img/unhappy/baby-2.jpg')}
                            style={styles.image} />
                    </View>
                </View>
                
                <View style={{margin: 20}}>
                    <Text style={styles.text}>    
                        <Text style={styles.textbold}>วิธีการนับลูกดิ้น</Text>
                        {'\n'}
                        {'\n'}{'     '}นับจำนวนเด็กดิ้นจนครบ 10 ครั้ง ในเวลา 4 ชั่วโมง ซึ่งนิยมให้นับในช่วงเช้า 
                        8.00-12.00 น.หากไม่ถึง 10 ครั้งต่อวัน ต้องรีบไปพบแพทย์ เพื่อรับการตรวจเพื่อหาสาเหตุเพิ่มเติม 
                        ควรสังเกตและจดบันทึกลูกดิ้นทุกวัน เพื่อติดตามสุขภาพทารกและป้องกันการเสียชีวิตของทารกในครรภ์ 
                        หากพบว่าลูกดิ้นน้อยลงอาจเกิดจากสาเหตุหลายประการ ได้แก่ การขาดออกซิเจน, การทำงานของระบบประสาทและกล้ามเนื้อลดลง, 
                        การทำงานของรกบกพร่องทำให้เลือดไปเลี้ยงลูกลดลง ถ้าลูกดิ้นมากแต่สม่ำเสมอ ถือว่าปกติ 
                        หากลูกดิ้นมาก ดิ้นอย่างรุนแรงแล้วหยุดดิ้นไปเลย มักจะเป็นสัญญาณของภาวะอันตรายอย่างเฉียบพลัน เช่น สายสะดือพันคอ
                    </Text>
                </View>
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    text: {
        fontFamily: 'NotoSansThai-Regular',
        fontSize: 15
    },
    textbold: {
        fontFamily: 'NotoSansThai-Bold',
        fontSize: 16
    },
    title: {
        fontFamily: 'NotoSansThai-Regular',
        fontSize: 16
    },
    touchBack: {
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        left: 8,
        top: 10,
        width: 50,
        height: 50,
        borderRadius: 23,
        backgroundColor: 'rgba(247, 247, 247, 0.5)'
    },
    textWhite: {
        fontFamily: 'NotoSansThai-Regular',
        fontSize: 15,
        color: 'white'
    },
    image: {
        height: 200,
        width: 250,
        margin: 5,
        borderRadius: 10
    }
})

export default Baby