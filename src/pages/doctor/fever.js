import React, { Component } from 'react'
import { View, Text, ScrollView, TouchableOpacity, StyleSheet, Image, ImageBackground } from 'react-native'

class Fever extends Component {

    onBack = () => {
        this.props.history.push('/doctor-menu')
    }

    render() {
        return (
            <ScrollView>
                <View style={{ flexDirection: 'row', backgroundColor: '#EECECF', height: 70, alignItems: 'center', justifyContent: 'center' }}>
                    <TouchableOpacity onPress={() => { this.onBack() }}
                        style={styles.touchBack}>
                        <Text style={styles.text}> กลับ </Text>
                    </TouchableOpacity>
                    <View style={{ alignItems: 'center' }}>
                        <Text style={styles.title}>มีไข้</Text>
                    </View>
                </View>

                <View style={{ margin: 20 }}>
                    <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                        <Image
                            source={require('../../img/unhappy/fever.jpg')}
                            style={styles.image} />
                    </View>
                </View>

                <View style={{margin: 20}}>
                    <Text style={styles.text}>
                        {'     '}เป็นอาการที่สัมพันธ์กับการติดเชื้อในร่างกายซึ่งมีผลต่อทารกในครรภ์ เช่น 
                        {'\n'}{'     '}{'     '}- หัดเยอรมัน ถ้าอยู่ในระยะ 3 เดือนแรก {'\n'}จะพบความพิการที่เห็นได้ชัดคือ ตาเป็นต้อกระจกหรือต้อหิน {'\n'}หูหนวก หัวใจพิการแต่กำเนิด 
                        {'\n'}{'     '}{'     '}- อีสุกอีใส ในระยะ 3 เดือนแรกของการตั้งครรภ์ อาจทำให้ทารกในครรภ์พิการได้
                    </Text>
                </View>

                <View style={{ margin: 20 }}>
                    <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                        <Image
                            source={require('../../img/unhappy/blood-2.jpg')}
                            style={styles.image} />
                    </View>
                </View>
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    text: {
        fontFamily: 'NotoSansThai-Regular',
        fontSize: 15
    },
    textbold: {
        fontFamily: 'NotoSansThai-Bold',
        fontSize: 16
    },
    title: {
        fontFamily: 'NotoSansThai-Regular',
        fontSize: 16
    },
    touchBack: {
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        left: 8,
        top: 10,
        width: 50,
        height: 50,
        borderRadius: 23,
        backgroundColor: 'rgba(247, 247, 247, 0.5)'
    },
    textWhite: {
        fontFamily: 'NotoSansThai-Regular',
        fontSize: 15,
        color: 'white'
    },
    image: {
        height: 200,
        width: 250,
        margin: 5,
        borderRadius: 10
    }
})

export default Fever