import React, { Component } from 'react'
import { View, Text, ScrollView, TouchableOpacity, StyleSheet, Image, ImageBackground } from 'react-native'

class Head extends Component {

    onBack = () => {
        this.props.history.push('/doctor-menu')
    }

    render() {
        return (
            <ScrollView>
                <View style={{ flexDirection: 'row', backgroundColor: '#EECECF', height: 70, alignItems: 'center', justifyContent: 'center' }}>
                    <TouchableOpacity onPress={() => { this.onBack() }}
                        style={styles.touchBack}>
                        <Text style={styles.text}> กลับ </Text>
                    </TouchableOpacity>
                    <View style={{ alignItems: 'center' }}>
                        <Text style={styles.title}>ปวดศีรษะ ตาพร่ามัว</Text>
                    </View>
                </View>

                <View style={{ margin: 20 }}>
                    <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                        <Image
                            source={require('../../img/unhappy/head.jpg')}
                            style={styles.image} />
                    </View>
                </View>

                <View style={{margin: 20}}>
                    <Text style={styles.text}>
                        {'     '}อาการปวดศีรษะ ตาพร่ามัว มือและเท้าบวม  และจุกแน่นใต้ลิ้นปี่ 
                        เป็นอาการนำของโรคความดันโลหิตสูงในขณะตั้งครรภ์ ที่อาจทำให้ทั้งมารดาและทารกในครรภ์เสียชีวิตได้ 
                        หากความดันโลหิตสูงโดยไม่ได้รับการรักษาอย่างทันท่วงที ความดันโลหิตจะเพิ่มสูงขึ้นอย่างต่อเนื่อง 
                        และผู้ป่วยจะเริ่มมีอาการปวดศีรษะมาก สายตาพร่ามัว จุกแน่นบริเวณลิ้นปี่ บวมหน้า มือข้อเท้า และเท้าจนถึงขั้นชักและหมดสติ  
                        หากมีเลือดออกในสมองอาจทำให้เสียชีวิตได้
                        มีเลือดออกทางช่องคลอดในระหว่างตั้งครรภ์ โดยเฉพาะเลือดสดๆ
                        
                    </Text>
                </View>
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    text: {
        fontFamily: 'NotoSansThai-Regular',
        fontSize: 15
    },
    textbold: {
        fontFamily: 'NotoSansThai-Bold',
        fontSize: 16
    },
    title: {
        fontFamily: 'NotoSansThai-Regular',
        fontSize: 16
    },
    touchBack: {
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        left: 8,
        top: 10,
        width: 50,
        height: 50,
        borderRadius: 23,
        backgroundColor: 'rgba(247, 247, 247, 0.5)'
    },
    textWhite: {
        fontFamily: 'NotoSansThai-Regular',
        fontSize: 15,
        color: 'white'
    },
    image: {
        height: 200,
        width: 250,
        margin: 5,
        borderRadius: 10
    }
})

export default Head