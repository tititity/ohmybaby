import React, { Component } from 'react'
import { View, Text, ScrollView, TouchableOpacity, StyleSheet, Image, ImageBackground } from 'react-native'
import { connect } from 'react-redux'

class DoctorMenu extends Component {

    onBack = () => {
        this.props.history.push('/main-menu')
    }

    onPress = (text) => {
        switch (text) {
            case 'WHITE':
                this.props.history.push('/doctor-white')
                break;
            case 'WATER':
                this.props.history.push('/water')
                break;
            case 'BLOOD':
                this.props.history.push('/blood')
                break;
            case 'FEVER':
                this.props.history.push('/fever')
                break;
            case 'HEAD':
                this.props.history.push('/doctor-head')
                break;
            case 'BABY':
                this.props.history.push('/baby')
                break;
            case 'PAIN':
                this.props.history.push('/pain')
                break;
        }
    }

    render() {
        return (
            <ScrollView>
                <View>
                    <ImageBackground
                        source={require('../../img/main-menu/doctor.png')} style={{ height: 93, width: 360 }}>
                        <TouchableOpacity style={styles.touchBack} onPress={() => this.onBack()}>
                            <Text style={styles.text}>กลับ</Text>
                        </TouchableOpacity>
                    </ImageBackground>
                </View>

                <View>
                    <TouchableOpacity
                        style={styles.menu}
                        onPress={() => { this.onPress('WHITE') }}>
                        <Text style={styles.text}>
                            ตกขาว-คัน
                        </Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={styles.menu}
                        onPress={() => { this.onPress('WATER') }}>
                        <Text style={styles.text}>
                            น้ำเดิน
                        </Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={styles.menu}
                        onPress={() => { this.onPress('BLOOD') }}>
                        <Text style={styles.text}>
                            เลือดออกทางช่องคลอด
                        </Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={styles.menu}
                        onPress={() => { this.onPress('FEVER') }}>
                        <Text style={styles.text}>
                            มีไข้
                        </Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={styles.menu}
                        onPress={() => { this.onPress('HEAD') }}>
                        <Text style={styles.text}>
                            ปวดศีรษะ ตาพร่ามัว
                        </Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={styles.menu}
                        onPress={() => { this.onPress('BABY') }}>
                        <Text style={styles.text}>
                            ลูกดิ้นลดลงหรือไม่ดิ้น
                        </Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={styles.menu}
                        onPress={() => { this.onPress('PAIN') }}>
                        <Text style={styles.text}>
                            เจ็บครรภ์คลอดก่อนกำหนด
                        </Text>
                    </TouchableOpacity>
                </View>
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    text: {
        fontFamily: 'NotoSansThai-Regular',
        fontSize: 15
    },
    textbold: {
        fontFamily: 'NotoSansThai-Bold',
        fontSize: 16
    },
    title: {
        fontFamily: 'NotoSansThai-Regular',
        fontSize: 16
    },
    touchBack: {
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        left: 8,
        top: 10,
        width: 50,
        height: 50,
        borderRadius: 23,
        backgroundColor: 'rgba(247, 247, 247, 0.5)'
    },
    menu: {
        height: 70, backgroundColor: '#EECECF', justifyContent: 'center', alignItems: 'center'
    }
})

export default connect(null)(DoctorMenu)