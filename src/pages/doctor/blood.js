import React, { Component } from 'react'
import { View, Text, ScrollView, TouchableOpacity, StyleSheet, Image, ImageBackground } from 'react-native'

class Blood extends Component {

    onBack = () => {
        this.props.history.push('/doctor-menu')
    }

    render() {
        return (
            <ScrollView>
                <View style={{ flexDirection: 'row', backgroundColor: '#EECECF', height: 70, alignItems: 'center', justifyContent: 'center' }}>
                    <TouchableOpacity onPress={() => { this.onBack() }}
                        style={styles.touchBack}>
                        <Text style={styles.text}> กลับ </Text>
                    </TouchableOpacity>
                    <View style={{ alignItems: 'center' }}>
                        <Text style={styles.title}>เลือดออกทางช่องคลอด</Text>
                    </View>
                </View>

                <View style={{ margin: 20 }}>
                    <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                        <Image
                            source={require('../../img/unhappy/blood-1.jpg')}
                            style={styles.image} />
                    </View>
                </View>

                <View style={{margin: 20}}>
                    <Text style={styles.text}>
                        มีเลือดออกทางช่องคลอดในระหว่างตั้งครรภ์ โดยเฉพาะเลือดสดๆ
                        {'\n'}
                        {'\n'}
                        <Text style={styles.textbold}>ข้อควรปฏิบัติ</Text>
                        {'\n'}
                        {'\n'}{'     '}1. ใส่ผ้าอนามัยรองซับไว้
                        {'\n'}{'     '}2. จดบันทึกเวลาที่เริ่มต้น 
                        {'\n'}{'     '}3. ให้รีบมาโรงพยาบาลทันที เพื่อให้ได้รับการรักษาต่อไป

                    </Text>
                </View>

                <View style={{ margin: 20 }}>
                    <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                        <Image
                            source={require('../../img/unhappy/blood-2.jpg')}
                            style={styles.image} />
                    </View>
                </View>
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    text: {
        fontFamily: 'NotoSansThai-Regular',
        fontSize: 15
    },
    textbold: {
        fontFamily: 'NotoSansThai-Bold',
        fontSize: 16
    },
    title: {
        fontFamily: 'NotoSansThai-Regular',
        fontSize: 16
    },
    touchBack: {
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        left: 8,
        top: 10,
        width: 50,
        height: 50,
        borderRadius: 23,
        backgroundColor: 'rgba(247, 247, 247, 0.5)'
    },
    textWhite: {
        fontFamily: 'NotoSansThai-Regular',
        fontSize: 15,
        color: 'white'
    },
    image: {
        height: 200,
        width: 250,
        margin: 5,
        borderRadius: 10
    }
})

export default Blood