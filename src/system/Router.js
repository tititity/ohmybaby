import React, { Component } from 'react'
import { Route, Switch, NativeRouter, Redirect } from 'react-router-native'
import MainMenu from '../pages/mainMenu';
import FoodMenu from '../pages/food/foodMenu';
import FoodDetail from '../pages/food/food-detail';
import FatDetail from '../pages/food/fat-detail';
import ProteinDetail from '../pages/food/protien-detail';
import VitaminDetail from '../pages/food/vitamin-detail';
import MineralDetail from '../pages/food/mineral-detail';
import CarboDetail from '../pages/food/carbo-detail';
import ExerciseMenu from '../pages/exercise/menu'
import LegAndAnkle from '../pages/exercise/leg-ankle';
import HeadAndNeck from '../pages/exercise/head-neck';
import Shoulder from '../pages/exercise/shoulder';
import SideMuscle from '../pages/exercise/side-muscle';
import Pelvis from '../pages/exercise/pelvis';
import Back from '../pages/exercise/back';
import Hip from '../pages/exercise/hip';
import Calf from '../pages/exercise/calf';
import Muscle from '../pages/exercise/muscle';
import Appointment from '../pages/appointment/appointment';
import WaterDetail from '../pages/food/water-detail';
import Login from '../pages/login';
import VideoPage from '../pages/exercise/video-page'
import AvoidDetail from '../pages/food/avoid-detail';
import WeightDetail from '../pages/food/weight-detail';
import growthMenu from '../pages/growth/growth-menu';
import MonthOne from '../pages/growth/month-1';
import MonthTwo from '../pages/growth/month-2';
import MonthThree from '../pages/growth/month-3';
import MonthFour from '../pages/growth/month-4';
import MonthFive from '../pages/growth/month-5';
import AddAppointment from '../pages/appointment/add-appointment';
import MonthSix from '../pages/growth/month-6';
import MonthSeven from '../pages/growth/month-7';
import MonthEight from '../pages/growth/month-8';
import MonthNine from '../pages/growth/month-9';
import UnhappyMenu from '../pages/unhappy/unhappy-menu';
import Sick from '../pages/unhappy/sick';
import White from '../pages/unhappy/white'
import Pee from '../pages/unhappy/pee';
import Stomach from '../pages/unhappy/stomach';
import Burn from '../pages/unhappy/burn';
import UnhappyBack from '../pages/unhappy/back'; 
import Cramp from '../pages/unhappy/cramp';
import Swell from '../pages/unhappy/swell';
import DoctorMenu from '../pages/doctor/doctor-menu';
import WhiteDoctor from '../pages/doctor/white';
import Water from '../pages/doctor/water';
import Blood from '../pages/doctor/blood'
import Fever from '../pages/doctor/fever';
import Head from '../pages/doctor/head';
import Baby from '../pages/doctor/baby';
import Pain from '../pages/doctor/pain';
 
export default class Router extends Component {
    render() {
        return (
            <NativeRouter>
                <Switch>
                    {/* <Route exact path="/login" component={Login}/> */}
                    <Route exact path="/main-menu" component={MainMenu} />
                    <Route exact path="/food-menu" component={FoodMenu} />
                    <Route exact path="/food-detail" component={FoodDetail} />
                    <Route exact path="/fat-detail" component={FatDetail} />
                    <Route exact path="/protein-detail" component={ProteinDetail} />
                    <Route exact path="/vitamin-detail" component={VitaminDetail} />
                    <Route exact path="/mineral-detail" component={MineralDetail}/>
                    <Route exact path="/carbo-detail" component={CarboDetail}/>
                    <Route exact path="/water-detail" component={WaterDetail}/>
                    <Route exact path="/avoid-detail" component={AvoidDetail}/>
                    <Route exact path="/weight-detail" component={WeightDetail}/>

                    <Route exact path="/exercise-menu" component={ExerciseMenu}/>
                    <Route exact path="/leg-ankle" component={LegAndAnkle}/>
                    <Route exact path="/head-neck" component={HeadAndNeck}/>
                    <Route exact path="/shoulder" component={Shoulder}/>
                    <Route exact path="/side-muscle" component={SideMuscle}/>
                    <Route exact path="/pelvis" component={Pelvis}/>
                    <Route exact path="/back" component={Back}/>
                    <Route exact path="/hip" component={Hip}/>
                    <Route exact path="/calf" component={Calf}/>
                    <Route exact path="/muscle" component={Muscle}/>
                    <Route exact path="/exercise-video" component={VideoPage}/>

                    <Route exact path="/appointment" component={Appointment}/>
                    <Route exact path="/add-appointment" component={AddAppointment}/>

                    <Route exact path="/growth-menu" component={growthMenu}/>
                    <Route exact path="/month1" component={MonthOne}/>
                    <Route exact path="/month2" component={MonthTwo}/>
                    <Route exact path="/month3" component={MonthThree}/>
                    <Route exact path="/month4" component={MonthFour}/>
                    <Route exact path="/month5" component={MonthFive}/>
                    <Route exact path="/month6" component={MonthSix}/>
                    <Route exact path="/month7" component={MonthSeven}/>
                    <Route exact path="/month8" component={MonthEight}/>
                    <Route exact path="/month9" component={MonthNine}/>

                    <Route exact path="/unhappy-menu" component={UnhappyMenu}/>
                    <Route exact path="/sick" component={Sick}/>
                    <Route exact path="/white" component={White}/>
                    <Route exact path="/pee" component={Pee}/>
                    <Route exact path="/stomach" component={Stomach}/>
                    <Route exact path="/burn" component={Burn}/>
                    <Route exact path="/unhappy-back" component={UnhappyBack}/>
                    <Route exact path="/cramp" component={Cramp}/>
                    <Route exact path="/swell" component={Swell}/>

                    <Route exact path="/doctor-menu" component={DoctorMenu}/>
                    <Route exact path="/doctor-white" component={WhiteDoctor}/>
                    <Route exact path="/water" component={Water}/>
                    <Route exact path="/blood" component={Blood}/>
                    <Route exact path="/fever" component={Fever}/>
                    <Route exact path="/doctor-head" component={Head}/>
                    <Route exact path="/baby" component={Baby}/>
                    <Route exact path="/pain" component={Pain}/>



                    <Redirect to = '/main-menu'/>
                </Switch>
            </NativeRouter>
        )
    }
}