import { createStore } from 'redux'
import reducers from '../reducers/reducers'

const store = createStore(reducers)
const state = store.getState()
console.log('state store',state)
export default store;