export const clickFat = (payload) => ({
    type: 'FAT',
    payload
})

export const clickPro = (payload) => ({
    type: 'PROTEIN',
    payload
})

export const clickVit = (payload) => ({
    type: 'VITAMIN',
    payload
})

export const clickMin = (payload) => ({
    type: 'MINEREAL',
    payload
})

export const clickCar = (payload) => ({
    type: 'CARBO',
    payload
})