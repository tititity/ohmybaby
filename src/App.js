import React, {Component} from 'react'
import { Provider } from 'react-redux'
import Router from './system/Router'
import store from './system/Store'

export default class App extends Component {
    render(){
        return(
            <Provider store = {store}>
                <Router/>
            </Provider>
        )
    }
}
