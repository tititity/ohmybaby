import firebase from 'firebase/app'
import 'firebase/app'
import 'firebase/database'
import 'firebase/auth'

const config = {
    apiKey: "AIzaSyAqpiDjjNRQAmSh-qK7SgVL2OFwA1OEpVE",
    authDomain: "oh-my-baby-d8d6b.firebaseapp.com",
    databaseURL: "https://example-everything.firebaseio.com",
    projectId: "oh-my-baby-d8d6b",
    storageBucket: "example-everything.appspot.com",
    messagingSenderId: "272861110863"
}

firebase.initializeApp(config)

const database = firebase.database()
const auth = firebase.auth()
const provider = new firebase.auth.FacebookAuthProvider()

export {
    database,
    auth,
    provider
}